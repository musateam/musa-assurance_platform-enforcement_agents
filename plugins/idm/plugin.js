
'use strict';

const IDM = require('./idm');

module.exports = function setup(options, imports, register,app) {
    const idm = new IDM();
    idm.setLoggerService(imports.log);
    idm.setEventsService(imports.events);
    idm.setJwtFactoryService(imports.jwtFactory);
    idm.setAgentFactoryService(imports.agentFactory);
    idm.setProxyFactoryService(imports.proxyFactory);
    idm.setApp(app);
    idm.setup(options);
    idm.init(function (error) {
        register(error, {});
    });
};