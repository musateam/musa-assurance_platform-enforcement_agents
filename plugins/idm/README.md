<!-- TOC -->

- [Plugin IDentity Manager](#plugin-identity-manager)
    - [Configuration](#configuration)
    - [Service](#service)
    - [Issue Reporting](#issue-reporting)
    - [Author](#author)
    - [License](#license)

<!-- /TOC -->
# Plugin IDentity Manager
MUSA IDentity Manager agent is a gateway to handle user lifecycle management. It provides a client to integrate with Identity Providers using OAuth2 and OpenID Connect protocols securely. 

It's provides the following features: 

 - Handle signup, signin, and signout processes 
 - Act as client in front of IDM providers 
 - Generate a JWT Token with user info inside it
 - JWT REST Service to verify token and sign payloads and return 

The only Identity Provider (idp) integrated yet is [Auth0](https://auth0.com/). This IDentity Provider could be use as a complete IDentity Provider, using Auth0 forms for signup, signin and signup process ans storing users on Auth0 storage. 

But if you are using your own user's storage (DB, LDAP, SAMLP IdentityProvider, MIcrosoft Azure AD,..) you can connect it to Auth0. Also you could extend with cool features like social signin ( google, twitter, facebook), passwordless (SMS, Email, TouchID)

## Configuration
------------
These are the main parts of MUSA IDentity Manager plugin configuration:

 - **cookie_secret**: secret to use on gateway session. Ex: "eljfwejewio282d4.3dPOLaw"
 - **cookie_name**: name to use on gateway session. Ex: "musag"
 - **idps**: An array with available authenticate providers. By default only Auth0 is available:
    - `name`: name of the provider. Ex: "auth0"
    - `idp_type`: the type of the idp. Ex: "auth0"
    - `domain`: the domain of the auth0 project. Ex: "musa.eu.auth0.com"
    - `clientID`: the clientID of the auth0 account. Ex: "rrG4OMeKxmgjadY1HZn2zX6KEAz7XnBl"
    - `clientSecret`: the clientID of the auth0 account. Ex: "hM9LSBYsw-U68xNsuDdAfjofslT3a2"
    - `signinPath`: the path to handle signin process. Ex: "/auth/auth0"
    - `signoutPath`: the path to handle signout process. Ex: "/auth/signout"
    - `passReqToCallback`: Ex: true
    - `callbackURL`: After the user authenticates we will only call back to any of these URLs. Ex: "https://idm-musa.enforcement.cu.cc:3000/callback"
    - `jwt`:  configuration of JWT Rest Service
        - **pem**: path to perm obtained from Auth0. Ex: "./idm/providers/musa.auth0.pem"
        - **cert**: path to cert obtained from Auth0. Ex: "./idm/providers/musa.auth0.cert"
        - **issuer**: url of the Auth0 JWT issuer. Ex: "https://musa.eu.auth0.com/
    - `webapps`: An array of client webapp integrated. For each webapps registered we will need:
        - **appID**: a client ID, each client will have a unique ID. Ex: clientID_1
        - **successRedirect**: callback to redirect browser when signin process is sucessfully done. Ex: "http://webapp-musa.enforcement.cu.cc:3001/auth/account" 
        - **failureRedirect**: callback to redirect browser when signin process is failure. Ex: "http://webapp-musa.enforcement.cu.cc:3001/auth/fail"
        - **signoutRedirect**: callback to redirect browser when signout process is sucessfully done. Ex: "http://webapp-musa.enforcement.cu.cc:3001/auth/post/signout"
    
## Service
------------
This service provides no API to be used by others services

## Issue Reporting
------------
If you have found a bug or if you have a feature request, please report them to admin email.

## Author
------------
[Borja Urquizu Tecnalia](mailto: borja.urquizu@tecnalia.com)

## License
------------
