/**
 * Created by urkizu on 7/27/16.
 */

"use strict";

var _ = require('underscore'),
    Strategy = require('passport-auth0-openidconnect').Strategy;

var Auth0Provider = module.exports = function Auth0Provider(idm) {
    //TODO: Check options
    this.name = 'auth0';
    this.idm = idm;
    this.passport = idm.passport;
    this.app = idm.app;
};

Auth0Provider.prototype = {
    checkProvider: function (provider) {
        //TODO: Check provider

    },
    init: function (provider) {
        var self = this;

        this.checkProvider(provider);

        this.passport.use(
            provider.name,
            new Strategy(
                _.defaults(
                    {
                        domain: provider.domain,
                        clientID: provider.clientID,
                        clientSecret: provider.clientSecret,
                        callbackURL: provider.callbackURL,
                        scope: provider.scope,
                        skipUserProfile: provider.skipUserProfile,
                        passReqToCallback: true
                    },
                    provider
                ),
                function (req, issuer, audience, profile, accessToken, refreshToken, params, done) {

                    console.log('issuer', issuer); // https://your-domain.auth0.com/
                    console.log('audience', audience); // user's id. i.e: auth0|5633afe0794d1c5a0b72a2be
                    console.log('accessToken', accessToken); // QSs...emeU
                    console.log('refreshToken', refreshToken); // gAUqAgTPr...dOquQxQ
                    console.log('params', params); // { access_token: '', id_token: '', token_type: '' }

                    req.info = {
                        userID: audience,
                        provider: provider.name,
                        params: params,
                        access_token: accessToken,
                        refresh_token: refreshToken
                    };
                    return done(undefined, req.info, params);
                }
            )
        );

        this.app.get(
            provider.signinPath,
            self.passport.authenticate(
                provider.name,
                _.defaults(
                    {
                        scope: provider.scope,
                        session: provider.session
                    },
                    provider.authOptions
                )
            )
        );
    }

};
