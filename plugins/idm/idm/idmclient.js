/**
 * Created by urkizu on 7/27/16.
 *
 *
 */

"use strict";

const AuthProviderManager = require('./provider');
const express = require('express');

var IDMClient = module.exports = function IDMClient(options) {
    this.manager = options.manager;
    this.app = options.app || express();
    this.apm = new AuthProviderManager({ app: this.app, webapps: options.webapps, manager: this.manager });
};

IDMClient.prototype = {
    verifyJWT: function (provider, token, options, callback) {
        return this.apm.verifyJWT(provider, token, options, callback);
    },
    setup: function (options) {
        var self = this;

        self.apm.init(options);

        options.idps.forEach(function (provider) {
            self.apm.configureProvider(provider);
        });

    }
};
