/**
 * Created by urkizu on 7/28/16.
 */

var url = require('url'),
    session = require('express-session'),
    _ = require('underscore');

/**
 * The passport configurator
 * @param {Object} app The LoopBack app instance
 * @constructor
 * @class
 */
var AuthProvider = module.exports = function AuthProvider(options) {
    var self = this;
    this.manager = options.manager;
    this.app = options.app;
    this.webapps = options.webapps;
    this.passport = require('passport');
    this.defaults = {};
    this.providers = {};
    this.defaults.successRedirect = function (successRedirect) {
        return function (req) {
            var returnTo;

            returnTo = successRedirect;

            if (req && req.session && req.session.clientID) {
                returnTo = self.webapps[req.session.clientID].root + self.webapps[req.session.clientID].successRedirect;
            }

            return returnTo + '?' +
                'userID=' + req.info.userID + '&' +
                'provider=' + req.info.provider + '&' +
                'access_token=' + req.info.access_token + '&' +
                'id_token=' + req.info.params.id_token;
        }
    };
    this.defaults.failureRedirect = function (failureRedirect) {
        return function (req) {
            var returnTo;

            returnTo = failureRedirect;

            if (req && req.session && req.session.clientID) {
                returnTo = self.webapps[req.session.clientID].root + self[req.session.clientID].failureRedirect;
            }

            // TODO: Add error to redirect
            return returnTo;
        }
    };
    this.defaults.defaultCallback = function (provider) {
        return function (req, res, next) {
            // The default callback
            self.passport.authenticate(provider.name, _.defaults({ session: provider.session }, provider.authOptions),
                function (err, user, info) {
                    if (err) {
                        return next(err);
                    }
                    if (!user) {
                        self.manager.events.emit("idm::signin::fail", Object.assign(self.manager.proxy.getRequestInfo(self.manager.services['proxy'], req), { provider: provider.name, user: {} }));
                        if (!!provider.json) {
                            return res.status(401).json("authentication error")
                        }
                        return res.redirect(provider.failureRedirect);
                    }
                    req.session.user = user;
                    self.manager.events.emit("idm::signin::success", Object.assign(self.manager.proxy.getRequestInfo(self.manager.services['proxy'], req), { provider: provider.name, user: user }));
                    return res.redirect(provider.successRedirectFunction(req));
                })(req, res, next);
        };
    };
    this.defaults.initCallback = function (req, res, next) {
        //TODO: Get client_id from header and add to session
        req.session.clientID = req.query.client_id;
        next();
    };
    this.defaults.finishCallback = function (provider) {
        return function (req, res) {
            var returnTo;

            returnTo = req.headers.referer + "/auth/signout" || provider.signoutRedirect;

            self.manager.events.emit("idm::signout::success", Object.assign(self.manager.proxy.getRequestInfo(self.manager.services['proxy'], req), { provider: provider.name, user: req.session.user }));

            if (req && req.session && req.session.clientID) {
                returnTo = self.webapps[req.session.clientID].root + self.webapps[req.session.clientID].signoutRedirect;
            }

            req.session.destroy();

            // TODO: Add error to redirect
            return res.redirect(returnTo);
        }
    };
};

AuthProvider.prototype = {

    verifyJWT: function (provider, token, options, callback, ctx) {

        if (_.isEmpty(provider)) {
            callback && callback.call(ctx || this, new Error("Unable to verify an empty provider."));
            return;
        }

        if (_.isEmpty(token)) {
            callback && callback.call(ctx || this, new Error("Unable to verify an empty token."));
            return;
        }

        if (!this.providers[provider] || !this.providers[provider].jwt) {
            callback && callback.call(ctx || this, new Error("Unable to get a jwt implementation for provider "));
            return;
        }

        this.manager.jwt.verify(this.providers[provider].jwt, token, options, callback, ctx);
    },
    /**
     * Initialize the passport configurator
     * @param {object} options
     *      object with the following options
     * @property {Boolean} session
     *      Set to true if session is required. defaults to false
     * @property {Array} providers
     *      An array of providers
     * @returns {Passport}
     */
    init: function (options) {

        if (_.isEmpty(options)) {
            options = {};
        }

        this.app.use(this.passport.initialize());
        this.app.use(
            session(
                {
                    secret: options.cookie_secret,
                    name: options.cookie_name,
                    proxy: true,
                    resave: true,
                    saveUninitialized: true
                }
            )
        );

        // Configure Passport authenticated session persistence.
        this.app.use(this.passport.session());
        // In order to restore authentication state across HTTP requests, Passport needs
        // to serialize users into and deserialize users out of the session.  In a
        // production-quality application, this would typically be as simple as
        // supplying the user ID when serializing, and querying the user record by ID
        // from the database when deserializing.
        this.passport.serializeUser(function (user, done) {
            done(null, user);
        });

        this.passport.deserializeUser(function (obj, done) {
            done(null, obj);
        });

        return this.passport;
    },
    /**
     * Configure a Passport strategy provider.
     * @provider {Object} General Options Options for the auth provider.
     * There are general provider that apply to all providers, and provider-specific
     * provider, as described below.
     * @property {String} name The provider name
     * @property {Object} module The passport strategy module from require.
     * @property {String} authScheme The authentication scheme, such as 'local',
     * 'oAuth 2.0'.
     * @property {Boolean} [session] Set to true if session is required.  Valid
     * for any auth scheme.
     * @property {String} [signinPath] Authentication route.
     *
     * @provider {Object} oAuth2 Options Options for oAuth 2.0.
     * @property {String} [clientID] oAuth 2.0 client ID.
     * @property {String} [clientSecret] oAuth 2.0 client secret.
     * @property {String} [callbackURL] oAuth 2.0 callback URL.
     * @property {String} [callbackURL] oAuth 2.0 callback route.
     * @property {String} [scope] oAuth 2.0 scopes.
     * @property {String} [successRedirect] The redirect route if login succeeds.
     * For both oAuth 1 and 2.
     * @property {String} [failureRedirect] The redirect route if login fails.
     * For both oAuth 1 and 2.
     *
     * @provider {Object} Local Strategy Options Options for local
     * strategy.
     * @property {String} [usernameField] The field name for username on the form
     * for local strategy.
     * @property {String} [passwordField] The field name for password on the form
     * for local strategy.
     *
     * @provider {Object} oAuth1 Options Options for oAuth 1.0.
     * @property {String} [consumerKey] oAuth 1 consumer key.
     * @property {String} [consumerSecret] oAuth 1 consumer secret.
     * @property {String} [successRedirect] The redirect route if login succeeds.
     * For both oAuth 1 and 2.
     * @property {String} [failureRedirect] The redirect route if login fails.
     * For both oAuth 1 and 2.
     *
     * @provider {Object} OpenID Options Options for OpenID.
     * @property {String} [returnURL] OpenID return URL.
     * @property {String} [realm] OpenID realm.
     * @end
     */
    configureProvider: function (provider) {
        var AuthProvider,
            customCallback;

        if (_.isEmpty(provider)) {
            provider = {};
        }

        provider.signinPath = provider.signinPath || ('/auth/' + provider.name);

        provider.callbackURL = provider.callbackURL || ('/auth/' + provider.name + '/callback');
        provider.callbackHTTPMethod = provider.callbackHTTPMethod !== 'post' ? 'get' : 'post';

        // remember returnTo position, set by ensureLoggedIn
        provider.successRedirectFunction = this.defaults.successRedirect(provider.successRedirect);
        provider.failureRedirectFunction = this.defaults.failureRedirect(provider.failureRedirect || '/login.html');

        AuthProvider = require('./providers/' + provider.name);

        // Register the path and the callback.
        this.app.all(url.parse(provider.signinPath).path, this.defaults.initCallback);
        this.app.all(url.parse(provider.signoutPath).path, this.defaults.finishCallback(provider, provider.signoutPath));

        // Add provider and init it
        this.providers[provider.name] = new AuthProvider(this);
        this.providers[provider.name].init(provider);

        // Register an instance of JWT
        this.providers[provider.name].jwt = this.manager.jwt.createJwt( 
            { 
                rest: 
                { 
                    app: this.app 
                } 
            } 
        );

        customCallback = provider.customCallback || this.defaults.defaultCallback(provider);

        // Register the path and the callback.
        this.app[provider.callbackHTTPMethod](url.parse(provider.callbackURL).path, customCallback);

    }
};


