#!/usr/bin/env node

/**
 * Created by urkizu on 7/29/16.
 */

const IDMClient = require('./idm/idmclient');
const url = require('url');

var IDM = module.exports = function IDM() {
    this.options = {};
    this.services = {};
    this.logger = undefined;
    this.agent = undefined;
    this.proxy = undefined;
    this.defaults = {};
    // WebApps registered
    this.webapps = {};
};

IDM.prototype = {
    setup: function (settings) {
        var webappURL,
            self = this;

        this.logger.info("IDM setup");
        // Store settings
        this.settings = JSON.parse(JSON.stringify(settings));
        // Setup IDM Client
        this.idmc.setup(settings);
        // Add webapps
        this.addWebApps(settings.webapps);
    },
    getSettings: function () {
        return { 
            'proxy': this.proxy.getSettings(this.services['proxy']),
            'agent': this.agent.getSettings(this.services['agent']),
            'idm': this.settings 
        };
    },
    setApp: function (app){
        this.app = app;
        this.app.on('start', this.start.bind(this));
        this.app.on('update', this.update.bind(this));
        this.app.on('stop', this.stop.bind(this));
    },
    setAgentFactoryService: function (agentFactory) {
        this.agent = agentFactory;
        this.services['agent'] = this.agent.createAgent();
        this.agent.setLoggerService(this.services['agent'], this.logger);
    },
    setProxyFactoryService: function (proxyFactory) {
        var self = this;

        this.proxy = proxyFactory;
        this.services['proxy'] = this.proxy.createProxy();
        this.proxy.addService(this.services['proxy'], { method: 'GET', url: '/' }, function( error, headers, req, res, next ) {
            res.render('index', { title: 'MUSA IDentity Manager Server', description: 'Default page for IDentity Manager Server' });
        });
        // Identity Management Client
        this.idmc = new IDMClient({ app: this.proxy.getApp(this.services['proxy']), webapps: this.webapps, manager: this });
    },
    setJwtFactoryService: function (jwtFactory) {
        this.jwt = jwtFactory;
    },
    setLoggerService: function (logger) {
        const self = this;

        this.logger = logger;
        this.logger.on( 'info', function( data) { self.events.emit( 'log::idm::info', { msg: data })});
        this.logger.on( 'warn', function( data) { self.events.emit( 'log::idm::warn', { msg: data })});
        this.logger.on( 'cerror', function( data) { self.events.emit( 'log::idm::error', { msg: data })});
    },
    setEventsService: function (events) {
        this.events = events;
    },
    addWebApps: function (webapps) {
        const self = this;
        //TODO: Añadir routas para enrutar las llamadas de los servicios
        //TODO: Registrar las rutas en express
        //TODO: Crear un cliente en Auth0 y registrar en el las callbacks
        Array.isArray(webapps) && webapps.forEach(function (webapp) {
            // KLUDGE: Reformat webapp config
            webappURL = url.parse(webapp.successRedirect);
            self.addWebApp({
                "id": webapp.appID,
                "name": webapp.appID,
                "root": webappURL.protocol + '//' + webappURL.host,
                "successRedirect": url.parse(webapp.successRedirect).path,
                "failureRedirect": url.parse(webapp.failureRedirect).path,
                "signoutRedirect": url.parse(webapp.signoutRedirect).path
            });
        });
    },
    addWebApp: function (webapp) {
        this.webapps[webapp.id] = webapp;
    },
    update: function(settings = {}, callback){
        settings[this.name] && ( this.setup(settings[this.name].options) );
        callback && callback();
    },
    start: function (settingd = {}, callback) {
        this.logger.info("IDM start");
        callback && callback();
    },
    stop: function (settings = {}, callback) {
        this.logger.info("IDM stop");
        callback && callback();
    },
    init: function (callback, ctx) {
        var self = this;

        this.agent.init(this.services['agent'], function (error) {
            if (error) {
                callback && callback.call(ctx || self, error);
                return;
            }
            callback && callback.call(ctx || self);
        });
    }
}





