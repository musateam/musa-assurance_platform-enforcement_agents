var Queue = require('./queue');
var Worker = require('./worker');

var queue = new Queue({ hosts: 'kafka.musa.org:2181' });
var worker = new Worker({ hosts: 'kafka.musa.org:2181' });

queue.start(function (error) {
    if (error) {
        console.error(error);
        process.exit()
    }
    worker.subscribe([{ topic: 'musa-agents-a1', partition: 1 }, { topic: 'musa-agents-a2', partition: 1 }], undefined, function (error, message, topic) {
        if (error) {
            console.error(error);
            process.exit();
        }
        console.log(message);
        switch (message.status) {
            case 'awaked':
                console.log(message);
                queue.enqueue({ topic: topic, message: { 'action': 'init', 'data': {} }, partition: 0 }, {}, function (error, result) {
                    if (error) {
                        console.error(error);
                        return;
                    }
                    console.log(message.status);
                });
                break;
            case 'inittiated':
                console.log(message);
                queue.enqueue({ topic: topic, message: { 'action': 'start' }, partition: 0 }, {}, function (error, result) {
                    if (error) {
                        console.error(error);
                        return;
                    }
                    console.log(message.status);
                });
                break;
            case 'started':
                console.log(message);
                setTimeout(function () {
                    queue.enqueue({ topic: topic, message: { 'action': 'disable' }, partition: 0 }, function (error, result) {
                        if (error) {
                            console.error(error);
                            return;
                        }
                        console.log(message.status);
                    });
                }, 10000);
                break;
            default:
                break;
        }
    });
})
