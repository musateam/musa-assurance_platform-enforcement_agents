# Plugins Broker
------------------
This plugin implements a message broker, but only act's as a client, it's need a message broker server. For now only kafka message broker is supported. 

In order to run properly we need a kafka message broker instance ready

### Worker
A worker is kafka client ready to subscribe/unsubscribe to topics. The worker instance with the following functions to call:

- **subscribe**: subscribe a topic or a list of topics 
    - `topics {array}`: a list of topics to subscribe. Each element could be 
        - *string*: only topic so an object it's autocreated *{ topic: <topic\>, partition: 0 }*
        - *{ topic: <topic\>, partition: <partition\> }*
    - `callback {function}`: function to call when done
    - `ctx {object}`: context to call with callback
    - `options {object}`: options to apply to consumer of events
```javascript
{
    //consumer group id, default `kafka-node-group`
    groupId: 'kafka-node-group',
    // Auto commit config
    autoCommit: true,
    autoCommitIntervalMs: 5000,
    // The max wait time is the maximum amount of time in milliseconds to block waiting if insufficient data is available at the time the request is issued, default 100ms
    fetchMaxWaitMs: 100,
    // This is the minimum number of bytes of messages that must be available to give a response, default 1 byte
    fetchMinBytes: 1,
    // The maximum bytes to include in the message set for this partition. This helps bound the size of the response.
    fetchMaxBytes: 1024 * 1024,
    // If set true, consumer will fetch message from the given offset in the payloads
    fromOffset: false,
    // If set to 'buffer', values will be returned as raw buffer objects.
    encoding: 'utf8'
}
```        

- **unsubscribe**: unsubdcribe to events 
    + `topics {array}`: a list of topics to unsubscribe. Each element could be 
        - *string*: only topic so an object it's autocreated *{ topic: <topic\>, partition: 0 }*
        - *{ topic: <topic\>, partition: <partition\> }*
    + `callback {function}`: function to call when done


### Queue

## Configuration
------------------
A JSON with the following properties:
```javascript
{
    // Hostname:port where zookeeper's kafka is located
    host: "kafka.enforcement.cu.cc:2181"
    // Topic to subscribe/emit messages
    topic: "musa.agents.e1",
    // Overwrite client default config
    client: {
        "client": {
            // SSL options to use on client
            "sslOptions": {
                //  put to false to admit self signed certificates
                "rejectUnauthorized": false,
                //  relative path where certs to connect using ssl are located
                "ca": "./plugins/broker/certs/kafka-ca.cert" 
            }    
        }      
    } 
}
```

## Service 
------------------
This service provides two main functions, one create a worker the lets you to subscribe/unsubscribe to topics and another one to create topics and emit events to topics

```javascript
/**
* Create a worker instance to subscribe/unsubscrube to events
* 
* @param config {object}
*      the configuration 
* @returns
*      the worker instance with the following functions to call
*          - subscribe: subscribe a topic or a list of topics 
*              + topics {array}: a list of topics to subscribe. Each element could be 
*                  - string: only topic so an object it's autocreated { topic: <topic>, partition: 0 }
*                  - { topic: <topic>, partition: <partition> }
*              + options {object}: options to apply to consumer of events 
*                   {
*                       groupId: 'kafka-node-group',//consumer group id, default `kafka-node-group`
*                       // Auto commit config
*                       autoCommit: true,
*                       autoCommitIntervalMs: 5000,
*                       // The max wait time is the maximum amount of time in milliseconds to block waiting if insufficient data is available at the time the request is issued, default 100ms
*                       fetchMaxWaitMs: 100,
*                       // This is the minimum number of bytes of messages that must be available to give a response, default 1 byte
*                       fetchMinBytes: 1,
*                       // The maximum bytes to include in the message set for this partition. This helps bound the size of the response.
*                       fetchMaxBytes: 1024 * 1024,
*                       // If set true, consumer will fetch message from the given offset in the payloads
*                       fromOffset: false,
*                       // If set to 'buffer', values will be returned as raw buffer objects.
*                       encoding: 'utf8'
*                   }
*              + callback {function}: function to call when done
*              + ctx {object}: context to call with callback
*          - unsubscribe: unsubdcribe to events 
*              + topics {array}: a list of topics to unsubscribe. Each element could be 
*                  - string: only topic so an object it's autocreated { topic: <topic>, partition: 0 }
*                  - { topic: <topic>, partition: <partition> }
*              + callback {function}: function to call when done
* 
*/
createWorker: function (config={}) {},
/**
* Create a queue instance to subscribe/unsubscrube to events 
* 
* @param config {object}
*      the configuration 
* @returns
*      the worker instance with the following functions to call
*          - enqueue: emit an event 
*              + data {object}: event to emit, must have the following format 
*                  { 
*                      topic: <topic>,
*                      partition: <partition>, // Default: 0 
*                      key: <key>, // only needed when using keyed partitioner
*                      message: { category: <category>, action: <action>, tag: <tag>, value: <data> }, 
*                  }
*              + options {object}: extra options to apply 
*                  {
*                      attributes: <attributes>// Default: 0  controls compression of the message set. It supports the following values: 0 No compresion, 1: Compress usin gzip, 2: Compress using snappy 
*                  } 
*              + callback {function}: function to call when done
*              + ctx {object}: context to call with callback
*          - createTopics: create a new topic
*              + topic {string||array}: name of the topic or an array of topics to create
*              + callback {function}: function to call when done
*/
createQueue: function (config={}) {}
```

## Issue Reporting
------------------
If you have found a bug or if you have a feature request, please report them to admin email.

## Author
------------------
[Borja Urquizu Tecnalia](mailto: borja.urquizu@tecnalia.com)

## License
