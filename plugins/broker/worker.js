
"use strict";

const kafka = require('kafka-node');
const _ = require('underscore');
const Consumer = kafka.Consumer;
const Offset = kafka.Offset;
const Client = kafka.Client;
const Job = require('./job');

var Worker = module.exports = function Worker(options = {}) {
    var self = this;
    this.offsets = [];
    this.subscribers = {};
    this.hosts = options.hosts || '127.0.0.1:2181';
    this.topic = options.topic || 'default';
    this.settings = {};
    this.settings.consumer = _.defaults(
        options.consumer || {},
        {
            groupId: 'musa-node-group',//consumer group id, default `kafka-node-group`
            autoCommit: true, // Auto commit config
            autoCommitIntervalMs: 5000,
            fetchMaxWaitMs: 100, // The max wait time is the maximum amount of time in milliseconds to block waiting if insufficient topics is available at the time the request is issued, default 100ms
            fetchMinBytes: 1, // This is the minimum number of bytes of messages that must be available to give a response, default 1 byte
            fetchMaxBytes: 1024 * 1024, // The maximum bytes to include in the message set for this partition. This helps bound the size of the response.
            fromOffset: false, // If set true, consumer will fetch message from the given offset in the payloads
            encoding: 'utf8' // If set to 'buffer', values will be returned as raw buffer objects.
        }
    );
    this.settings.client = _.defaults(
        options.client || {},
        {
            connectionString: this.hosts, //Zookeeper connection string, default localhost:2181
            clientId: 'musa-agent' //This is a user-supplied identifier for the client application, default kafka-node-client
        }
    );
    this.client = new Client(
        this.settings.client.connectionString,
        this.settings.client.clientId,
        this.settings.client.zkOptions,
        this.settings.client.noAckBatchOptions,
        this.settings.client.sslOptions
    );
    this.client.on('connect', function () {
        self.logger.info("Worker host: " + self.hosts + " stablished.");
    });
    this.client.on('error', function (error) {
        self.logger.error("Worker host: " + self.hosts + " error " + error);
    });
    this.offset = new kafka.Offset(this.client);
};

Worker.prototype = {
    setLoggerService: function (logger) {
        this.logger = logger;
    },

    subscribe: function (topics, options = {}, callback, ctx) {
        var self = this,
            topicToSubscribe;
        try {
            if (typeof topics == 'string') {
                topics = [topics];
            }
            topicToSubscribe = [];
            topics.forEach(function (topic) {
                if (typeof topic === 'string') {
                    topicToSubscribe.push({ topic: topic, partition: 0 });
                } else {
                    topicToSubscribe.push(topic);
                }
            });
            var consumer = new Consumer(this.client, topicToSubscribe, options);
            self.subscribers[topics] = consumer;
            consumer.on('message', function (message) {
                try {
                    if (self.offsets.indexOf(message.offset) != -1) {
                        self.logger.error("Duplicate offset " + message.offset);
                        return;
                    }
                    self.offsets.push(message.offset)
                    var msg = JSON.parse(message.value)
                    callback && callback.call(ctx || this, undefined, msg.data, message.topic, message);
                    self.logger.debug("Received message offset: " + message.offset + " topic: " + message.topic + " partition: " + message.partition + " key: " + message.key + " message: " + message.value);
                } catch (error) {
                    callback && callback.call(ctx || this, error);
                }
            });
            consumer.on('error', function (err) {
                callback && callback.call(ctx || this, err);
            });
            /*
            * If consumer get `offsetOutOfRange` event, fetch topics from the smallest(oldest) offset
            */
            consumer.on('offsetOutOfRange', function (topic) {
                topic.maxNum = 2;
                self.offset.fetch([topic], function (error, offsets) {
                    if (error) {
                        callback && callback.call(ctx || this, error);
                        return;
                    }
                    var min = Math.min(offsets[topic.topic][topic.partition]);
                    consumer.setOffset(topic.topic, topic.partition, min);
                });
            });
        } catch (error) {
            callback && callback.call(ctx || this, error);
        }
    },
    unsubscribe: function (topics, callback) {
        this.subscribers[topics].close(true, callback);
    }
}
