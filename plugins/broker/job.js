
"use strict";

const uuidV4 = require('uuid/v4');

var Job = module.exports = function Job(topic, data, key = null) {
    if (!topic || !data) {
        throw new Error('Unable to create job with invalid data.');
    }
    this.id = uuidV4();
    this.timestamp = Date.now();
    this.topic = topic;
    this.data = data;
    this.key = key;
};

Job.prototype = {
    toJSON: function () {
        return JSON.stringify(
            {
                id: this.id,
                timestamp: this.timestamp,
                topic: this.topic,
                data: this.data,
                key: this.key
            }
        );
    }

}