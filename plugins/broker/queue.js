
"use strict";

const kafka = require('kafka-node');
const _ = require('underscore')
const Producer = kafka.Producer;
const Client = kafka.Client;
const Job = require('./job');

var Queue = module.exports = function Queue(options = {}) {
    const self = this;
    this.hosts = options.hosts || '127.0.0.1:2181';
    this.topic = options.topic || 'default';
    this.settings = {};
    this.settings.producer = _.defaults(
        options.producer || {},
        {
            requireAcks: 1, // Configuration for when to consider a message as acknowledged, default 1
            ackTimeoutMs: 100, // The amount of time in milliseconds to wait for all acks before considered, default 100ms
            partitionerType: 3 // Partitioner type (default = 0, random = 1, cyclic = 2, keyed = 3), default 0
        }
    );
    if( options.client && options.client.ca ) { 
        // Get ca certificate from path
        options.client.ca = require('fs').readFileSync(require('path').join(__dirname, options.sslOptions.ca), 'utf8');
    }
    this.settings.client = _.defaults(
        options.client || {},
        {
            connectionString: this.hosts, //Zookeeper connection string, default localhost:2181
            clientId: 'musa-agent' //This is a user-supplied identifier for the client application, default kafka-node-client
        }
    );
    this.client = new Client(
        this.settings.client.connectionString,
        this.settings.client.clientId,
        this.settings.client.zkOptions,
        this.settings.client.noAckBatchOptions,
        this.settings.client.sslOptions
    );
    this.client.on('connect', function () {
        self.logger.info("Queue host: " + self.hosts + " stablished.");
    });
    this.client.on('error', function (error) {
        self.logger.error("Queue host: " + self.hosts + " error " + error);
    });
};

Queue.prototype = {
    setLoggerService: function (logger) {
        this.logger = logger;
    },
    enqueue: function (data, options = {}, callback, ctx) {
        const self = this;
        const msgTopics = {};
        const topicsToCreate = [];
        const msgs = [];
        if (!Array.isArray(data)) {
            data = [data];
        }
        data.forEach(function (topicData) {
            msgs.push(
                {
                    topic: topicData.topic || self.topic,
                    partition: topicData.partition || 0, // default 0
                    key: options.key, // only needed when using keyed partitioner
                    messages: new Job(topicData.topic || self.topic, topicData.message, topicData.key || null).toJSON(),  // multi messages should be a array, single message can be just a string or a KeyedMessage instance
                    attributes: options.attributes || 0 //  controls compression of the message set. It supports the following values: 0 No compresion, 1: Compress usin gzip, 2: Compress using snappy
                }
            );
            topicsToCreate.push(topicData.topic || self.topic);
            msgTopics[topicData.topic || self.topic] = JSON.stringify(topicData.message);
        });
        this.producer.createTopics(topicsToCreate, true, function (err, data) {
            if (err) {
                callback && callback.call(ctx || this, err);
                return;
            }
            try {
                self.producer.send(msgs, function (error, result) {
                    if (error) {
                        callback && callback.call(ctx || self, err);
                        return;
                    }
                    Object.keys(result).forEach(function (topic) {
                        Object.keys(result[topic]).forEach(function (partition) {
                            self.logger.debug("Send message topic: " + topic + " partition: " + partition + " offset " + result[topic][partition] + " message: " + msgTopics[topic]);
                        });
                    });
                    callback && callback.call(ctx || self, undefined, result);
                }
                );
            } catch (error) {
                callback && callback.call(ctx || this, error);
            }
        });
    },
    createTopics: function (topic, callback) {
        this.producer.createTopics([topic], true, callback);
    },
    start: function (callback) {
        try {
            this.producer = new Producer(this.client, this.settings.producer);
            // this event is emitted when producer is ready to send messages.
            this.producer.on('ready', callback);
            // is is the error event propagates from internal client, producer should always listen it.
            this.producer.on('error', callback);
        } catch (error) {
            callback && callback.call(ctx || this, error);
        }
    },
    stop: function (callback) {
        try {
            this.client.close(callback);
        } catch (error) {
            callback && callback.call(ctx || this, error);
        }
    }
}
