var Worker = function Worker(){};
Worker.prototype = {
    setLoggerService: function (logger) {
        this.logger = logger;
    },

    subscribe: function (topics, options = {}, callback, ctx) {
    },
    unsubscribe: function (topics, callback) {
    }
}

var Queue = function Queue(){};
Queue.prototype = {
    setLoggerService: function (logger) {
        this.logger = logger;
    },
    enqueue: function (data, options = {}, callback, ctx) {
        callback && callback.call(ctx || this);
    },
    createTopics: function (topic, callback) {
        callback && callback();
    },
    start: function (callback) {
        callback && callback();
    },
    stop: function (callback) {
        callback && callback();
    }        
}
    
exports.Worker = Worker;
exports.Queue = Queue;