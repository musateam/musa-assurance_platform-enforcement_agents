const repl = require('repl');
const BClient = require('./bclient');

const r = repl.start({ prompt: '> ' });
r.context.broker = new BClient();
r.context.broker.start();

