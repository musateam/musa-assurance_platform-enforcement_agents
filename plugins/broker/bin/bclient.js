const repl = require('repl');
const Queue   = require('../queue');
const Worker  = require('../worker');
const config = require('./config.json');

var KafkaClient = module.exports = function KafkaClient(){
    this.hosts = config.hosts;
    this.logger = {
        debug: console.log,
        info: console.log,
        error: console.error
    };
    this.topic = config.topic;
    this.partition = config.partition;
    this.client = config.client;
};

KafkaClient.prototype = {
    createTopics: function (topic) {
        const that = this;

        this.queue.createTopics([topic], function ( err, data) {
            if (error) {
                that.logger.error(error);
                process.exit();
            }
            that.logger.debug('Topics created ' + JSON.stringify(topics) + ' with data ' + JSON.stringify(data));                        
        });
    },
    subscribe: function(topic=this.topic, partition=this.partition){
        const that = this;
        this.worker.subscribe([{ topic: topic, partition: partition }], undefined, function (error, message, topic) {
            if (error) {
                that.logger.error(error);
                process.exit();
            }
            that.logger.debug('New msg: ' + JSON.stringify(message));
        });
    },
    emit: function( message={}, topic=this.topic, partition=this.partition){
        const that = this;
        this.queue.enqueue({ topic: topic, message: message, partition: partition }, {}, function (error, result) {
            if (error) {
                that.logger.error(error);
                return;
            }
            that.logger.debug(result);
        });
    },
    start: function (){
        const that = this;
        this.queue = new Queue({ hosts: this.hosts, client: this.client });
        this.worker = new Worker({ hosts: this.hosts, client: this.client });
        this.queue.setLoggerService(this.logger);
        this.worker.setLoggerService(this.logger);
        this.queue.start(function (error) {
            if (error) {
                that.logger.error(error);
                process.exit();
            }
            that.logger.info("Successfully started");
        })        
    }
}




