
'use strict';

const Agent = require('./agent');

module.exports = function setup(options, imports, register, app) {
    var agents = {};
    var i = 0;
    /*var defaultAgent ={};
    defaultAgent = new Agent();
    defaultAgent.setBrokerService(imports.broker);
    defaultAgent.setLoggerService(imports.log);
    defaultAgent.setEventsService(imports.events);
    defaultAgent.setApp(app);
    defaultAgent.setup(options);*/

    register(
        null, // Error
        {
            //agent: defaultAgent,
            agentFactory: {
                /**
                 * Create an instance of an agent and return it's id
                 * @param config {object}
                 *      the configuration 
                 * @returns
                 *      the new agent id
                */
                createAgent: function (config = {}) {
                    var agent;
                    agent = new Agent(config);
                    agent.setBrokerService(imports.broker);
                    agent.setLoggerService(imports.log);
                    agent.setEventsService(imports.events);
                    agent.setApp(app);
                    agent.setup(imports.util.deepMerge(options, config))
                    agents[i] = agent;
                    return i++;
                },
                /**
                 * Init the given agent
                 * 
                 * @param agent {string}
                 *      the agent id returned by createAgent 
                 * @param callback {function}
                 *      the function to call once an agent is initiated
                 * @param ctx {object}
                 *      the context to apply with callback
                */
                init: function (agent, callback, ctx) {
                    return agents[agent].init.call(agents[agent], callback, ctx);
                },
                /**
                 * Associate an app to an agent
                 * 
                 * @param agent {string}
                 *      the agent id returned by createAgent 
                 * @param app {object}
                 *      the app must implement the following functions
                 *          - start(settings) start app. Fired on start transition. It receives a json with the config to take into account 
                 *          - stop() stop app. Fired on stop transition.
                 *          - update(settings) reload app. Fired on stop transition. It receives a json with the config to take into account on reload
                */
                setApp: function (agent, app) {
                    return agents[agent].setApp.call(agents[agent], app);
                },
                /**
                 * Returns the settings of an agent
                 * 
                 * @param agent {string}
                 *      the agent id returned by createAgent 
                 */
                getSettings: function (agent) {
                    return agents[agent].getSettings.call(agents[agent]);
                },
                /**
                 * Setup the agent
                 * 
                 * @param agent {string}
                 *      the agent id returned by createAgent 
                 * @param options {object}
                 *      the options to apply to agent
                 * 
                 */
                setup: function (agent, options = {}) {
                    return agents[agent].setup.call(agents[agent], options);
                },
                /**
                 * Set the broker service to use
                 * 
                 * @param agent {string}
                 *      the agent id returned by createAgent 
                 * @param broker {object}
                 *      the broker service
                 * 
                */
                setBrokerService: function (agent, broker) {
                    return agents[agent].setBrokerService.call(agents[agent], broker);
                },
                /**
                 * Set the logger service to use
                 * 
                 * @param agent {string}
                 *      the agent id returned by createAgent 
                 * @param logger {object}
                 *      the logger service
                 * 
                */
                setLoggerService: function (agent, logger) {
                    return agents[agent].setLoggerService.call(agents[agent], logger);
                },
                /**
                 * Subscribe to events emitted by agent
                 * 
                 * @param agent {string}
                 *      the agent id returned by createAgent 
                 * @param event {object}
                 *      the event to subscribe
                 *          - 'change_state': when state changes, as value an object with 'previous' state and 'current' state
                 *          - state: when state changes, an event is emitted with state as topic and 'previous' state as value
                 */
                on: function (agent, event, callback) {
                    return agents[agent].on.call(agents[agent], event, callback);
                },
                /**
                 * Transition and agent
                 * 
                 * @param agent {string}
                 *      the agent id returned by createAgent 
                 * @param action {string}
                 *      the action to transition 
                 * 
                 */
                doAction: function (agent, action) {
                    return agents[agent].doAction.call(agents[agent], action);
                }
            }
        }
    );
};