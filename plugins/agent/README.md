<!-- TOC -->

- [Plugins Agent](#plugins-agent)
        - [Associated app](#associated-app)
        - [States](#states)
        - [Transitions](#transitions)
        - [Events](#events)
    - [Configuration](#configuration)
    - [Service](#service)
        - [agent](#agent)
        - [agentFactory](#agentfactory)
    - [Issue Reporting](#issue-reporting)
    - [Author](#author)
    - [License](#license)

<!-- /TOC -->
# Plugins Agent
------------------
This plugins implements an simple agent. An agent is basically a Finite State Machine with transitions and callbacks associated to every transition also it's emit and subscribe to public events using **broker service**. 

### Associated app
Every agent could have associated an app, in some transitions the app associted is called to inform so the associated app must implements an interface:

 - **start**: start app. Fired on start transition. It receives a json with the config to take into account 
 - **stop**: stop app. Fired on stop transition.
 - **update**: update app. Fired on stop transition. It receives a json with the config to take into account on reload

### States 
The states are the following 

 - **initial**: initial state 
 - **idle**: the agent is idle and awaiting to awake
 - **awaked**: the agent is awaked
 - **inittiated**: the agent is inittiated
 - **started**: the agent is started
 - **stopped**: the agent is stopped

### Transitions
The defined transitions for an agent are the following:
```javascript
Transitions: [
    { action: "initial", from: "initial", to: "idle" },
    { action: "awake", from: "idle", to: "awaked" },
    { action: "init", from: "awaked", to: "inittiated" },
    { action: "start", from: "inittiated", to: "started" },
    { action: "update", from: "started", to: "started" },
    { action: "stop", from: "started", to: "stopped" },
    { action: "start", from: "stopped", to: "started" }    
]
```
The available transitions are:

 - **initial**: initial  
 - **awake**: awake and agent
 - **init**: init an agent
 - **update**: update the config of an agent
 - **start**: start an agent
 - **stop**:  stop an agent

It's started in 'initial' state by default but could be parametrized by configuration. 

There two ways to make a transition in an agent:

 - using service's function *doAction*
 - emiting a public event using **broker service** to a topic subscribed by this agent (ex: 'musa.agents.e1'). This event must have the following format:
    - 'agent' string as *category*
    - 'state' string as *action*
    - 'action' string as *label*
    - { trigger: <transitionToMake>, data: <extraData> } JSON as *value*

When plugin is initiated the *initial* transition is fired

### Events
By default every agent is subscribed to topic <prefix><agentID>, ex: 'musa.agents.e1'
It's uses **broker service** to emit events outside app, these events have 4 properties:

 - **category**: the name you supply for the group of objects you want to track.
 - **action**: a string that is uniquely paired with each category.
 - **label**: an optional string to provide additional dimensions to the event data.
 - **value**: a json that you can use to provide extra data about the event.

This agent emits public events throught **broker service**. The events emitted by every agent are:

 - On every transition a public event is emitted 
    - 'agent' string as category
    - 'state' string as action
    - 'action' string as label
    - { state: 'idle', trigger: 'idle', data: {} } JSON as value
 - On any local events emited by **events service** is retransmited as a public event, an event format 'category::action::label::value' is splitted using each value  

## Configuration
------------------
A JSON with the following properties:

 - **agentID** {string}: unique ID of the agent. Required
 - **type** {string}: the type of the agent, it not used by agent. Default: None
 - **initialState** {string}: the initial state of the internal Finite State Machine of the agent. Default 'initial'
 - **prefix** {string}: The prefix to append to topics subscribed and emitted by agent. Default 'musa.agents'
 - **keepAliveInterval** {integer}: interval to emit a keep alive event

## Service 
------------------
This plugin exposes two services  

### agent
This service provides an agent with the app descriptor configuration inside agent plugin configuration ( or default configuration if not present ), all others plugins inside the same app shares this instance

### agentFactory 
This service uses Factory pattern to let to create your own implementation of an agent not shared with other plugins or using custom configuration:

```javascript
/**
    * Create an instance of an agent and return it's id
    * @param config {object}
    *      the configuration 
    * @returns
    *      the new agent id
*/
createAgent: function (config = {}) {},
/**
    * Init the given agent
    * 
    * @param agent {string}
    *      the agent id returned by createAgent 
    * @param callback {function}
    *      the function to call once an agent is initiated
    * @param ctx {object}
    *      the context to apply with callback
*/
init: function (agent, callback, ctx) {},
/**
    * Associate an app to an agent
    * 
    * @param agent {string}
    *      the agent id returned by createAgent 
    * @param app {object}
    *      the app must implement the following functions
    *          - start(settings) start app. Fired on start transition. It receives a json with the config to take into account 
    *          - stop() stop app. Fired on stop transition.
    *          - update(settings) reload app. Fired on stop transition. It receives a json with the config to take into account on reload
*/
setApp: function (agent, app) {},
/**
    * Returns the settings of an agent
    * 
    * @param agent {string}
    *      the agent id returned by createAgent 
    */
getSettings: function (agent) {},
/**
    * Setup the agent
    * 
    * @param agent {string}
    *      the agent id returned by createAgent 
    * @param options {object}
    *      the options to apply to agent
    * 
    */
setup: function (agent, options = {}) {},
/**
    * Set the broker service to use
    * 
    * @param agent {string}
    *      the agent id returned by createAgent 
    * @param broker {object}
    *      the broker service
    * 
*/
setBrokerService: function (agent, broker) {},
/**
    * Set the logger service to use
    * 
    * @param agent {string}
    *      the agent id returned by createAgent 
    * @param logger {object}
    *      the logger service
    * 
*/
setLoggerService: function (agent, logger) {},
/**
    * Subscribe to events emitted by agent
    * 
    * @param agent {string}
    *      the agent id returned by createAgent 
    * @param event {object}
    *      the event to subscribe
    *          - 'change_state': when state changes, as value an object with 'previous' state and 'current' state
    *          - state: when state changes, an event is emitted with state as topic and 'previous' state as value
    */
on: function (agent, event, callback) {},
/**
    * Transition and agent
    * 
    * @param agent {string}
    *      the agent id returned by createAgent 
    * @param action {string}
    *      the action to transition 
    * 
    */
doAction: function (agent, action) {}
```
                
## Issue Reporting
------------------
If you have found a bug or if you have a feature request, please report them to admin email.

## Author
------------------
[Borja Urquizu Tecnalia](mailto: borja.urquizu@tecnalia.com)

## License
