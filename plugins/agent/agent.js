const async = require('async');
const os = require('os');
const _ = require('underscore');
const FSM = require('./fsm');

var AgentManagement = module.exports = function AgentManagement() {
    const self = this;
    this.queue = undefined;
    this.logger = undefined;
    this.fieldsToExcludeOnAppData = ["consumes", "destroy", "packagePath", "provides", "setup"];
    this.populateStatus = function (data) {
        self.logger.info("State changed " + JSON.stringify(data));
    }
    this.onEvent = function (event, data, options={}) {
        const type = event.split('::');
        setImmediate(function () {
            self.sendMessage(type[0], type[1], type[2], data, options);
        });
    }

    this.sendMessage = function (category, action, tag, data, options={}, callback) {
        try {
            self.queue.enqueue({ message: { category: category, action: action, tag: tag, value: data }, partition: options.partition || 1 }, {}, function (error, result) {
                if (error) {
                    self.logger.error(error);
                    callback && callback(error);
                    return;
                }
                self.logger.debug(result);
                callback && callback(undefined, result);
            });
        } catch (error) {
            self.logger.error(error + " sending activity msg");
        }
    };
    this.onMessage = function (error, msg) {
        if (error) {
            self.logger.error(error.stack + " subscribing to queue.");
            return;
        }
        switch (msg.category) {
            case 'agent':
                self.agentMessageHandle(msg.action, msg.tag, msg.value);
                break;
            default:
                self.logger.warn(JSON.stringify(msg) + " discarted.");
                break;
        }
    };
    this.agentMessageHandle = function (action, tag, value) {
        switch (action) {
            case 'state':
                switch (tag) {
                    case 'action':
                        self.fsm.machine.process(value.trigger, value.data);
                        break;
                    default:
                        self.logger.warn("action:" + action + " tag:" + tag + " value:"+ JSON.stringify(value) + " discarted.");
                        break;
                }
                break;
            default:
                break;
        }
    };
};

AgentManagement.prototype = {
    setApp: function (app) {
        // TODO: Ensure interface it's implemented
        this.app = app;
        this.app.on('update', this.update.bind(this));
    },
    update: function (data, callback, ctx){
        callback && callback.call(ctx || this);
    },
    setup: function (options = {}) {
        var self = this;
        //TODO: Sanity input via schemas
        this.name = options.name;
        this.options = options;
        this.agentID = options.agentID || "MUSAAgent" + options.type + Math.round(Math.random()*10) + 1;
        this.fsm = {};
        this.fsm.stateCallbacks =
        {
            onIdle: function (action, data = "{}", done) {             
                var sys = {};
                sys['system'] = {                            
                    'platform': os.platform(),
                    'hostname': os.hostname(),
                    'network': os.networkInterfaces( )    
                }; 
                self.sendMessage('agent', 'state', 'action', { state: 'idle', trigger: 'idle', data: Object.assign({}, self.app.settings.plugins, sys )}, {}, done);
            },
            onAwaked: function (action, data = "{}", done) {
                self.sendMessage('agent', 'state', 'action', { state: 'awaked', trigger: 'awaked' }, {}, done);
            },
            onInit: function (action, data = "{}", done) {
                self.app.update(JSON.parse(data), function (error) {
                    if (error) {
                        done(error);
                        return;
                    }
                    self.sendMessage('agent', 'state', 'action', { state: 'inittiated', trigger: 'inittiated' }, {}, done);
                });
            },
            onStart: function (action, data = "{}", done) {
                self.app.start(JSON.parse(data), function (error) {
                    if (error) {
                        done(error);
                        return;
                    }
                    self.sendMessage('agent', 'state', 'action', { state: 'started', trigger: 'started' }, {}, done);
                });
            },
            onStop: function (action, data = "{}", done) {
                self.app.stop(JSON.parse(data), function (error) {
                    if (error) {
                        done(error);
                        return;
                    }
                    self.sendMessage('agent', 'state', 'action', { state: 'stopped', trigger: 'stopped' }, {}, done);
                });
            },
            onUpdate: function (action, data = "{}", done) {
                self.app.update(JSON.parse(data), function (error) {
                    if (error) {
                        done(error);
                        return;
                    }
                    self.sendMessage('agent', 'state', 'action', { state: 'updated', trigger: 'updated', data: self.app.settings.plugins }, {}, done);
                });
            }
        };
        this.fsm.states =
        [
            { action: "initial", from: "initial", to: "idle", callback: this.fsm.stateCallbacks.onIdle },
            { action: "awake", from: "idle", to: "awaked", callback: this.fsm.stateCallbacks.onAwaked },
            { action: "init", from: "awaked", to: "inittiated", callback: this.fsm.stateCallbacks.onInit },
            { action: "update", from: "started", to: "started", callback: this.fsm.stateCallbacks.onUpdate },
            { action: "update", from: "inittiated", to: "inittiated", callback: this.fsm.stateCallbacks.onUpdate },
            { action: "update", from: "stopped", to: "stopped", callback: this.fsm.stateCallbacks.onUpdate },
            { action: "start", from: "inittiated", to: "started", callback: this.fsm.stateCallbacks.onStart },
            { action: "stop", from: "started", to: "stopped", callback: this.fsm.stateCallbacks.onStop },
            { action: "start", from: "stopped", to: "started", callback: this.fsm.stateCallbacks.onStart }
        ];
        if( !options.initialState ){
            options.initialState = [["initial", "idle"]];
        }
        this.fsm.initial = options.initialState[0][0];
        this.keepAliveInterval = options.keepAliveInterval || 30000;
    },
    getSettings: function (){
        return this.options;
    },
    setBrokerService: function (broker) {
        this.broker = broker;
    },
    setLoggerService: function (logger) {
        this.logger = logger;
    },
    setEventsService: function (events) {
        this.events = events;
    },
    on: function (event, callback) {
        this.fsm.machine.on(event, callback);
    },
    once: function (event, callback) {
        this.fsm.machine.once(event, callback);
    },
    doAction: function (trigger, data = "{}") {
        this.fsm.machine.process(trigger, data);
    },
    init: function (callback, ctx) {
        const self = this;
        this.topic = (this.options.prefix || 'musa.agents.') + this.agentID;
        this.fsm.machine = new FSM(this);

        // Queue
        async.waterfall(
            [
                function setupFSM(cb) {
                    var states = [];
                    self.fsm.states.forEach(function (actionCallback) {
                        states.push(actionCallback.to);
                        self.fsm.machine.addTransition(actionCallback.action, actionCallback.from, actionCallback.to, actionCallback.callback || function (action, data, done) { done(); })
                    });
                    self.fsm.machine.setInitialState(self.fsm.initial);
                    states.push(self.fsm.initial);
                    states = _.uniq(states);
                    states.forEach(function (state) {
                        self.fsm.machine.on(state, self.populateStatus);
                    });
                    cb();
                },
                function setupBroker(cb) {
                    self.worker = self.broker.createWorker();
                    self.queue = self.broker.createQueue({ topic: self.topic });
                    self.queue.start(cb);
                },
                function start(cb) {
                    self.queue.createTopics(self.topic, function (error) {
                        if (error) {
                            cb(error);
                            return;
                        }
                        self.worker.subscribe([{ topic: self.topic, partition: 0 }], undefined, self.onMessage);
                        cb();
                    });
                },
                function subscribeToEvents(cb) {
                    try {
                        self.events.onAny(function (event, value, options) { self.onEvent(this.event, value, options); });
                        cb();
                    } catch (error) {
                        cb(error);
                    }
                },
                function keepAliveMsg(cb) {
                    setInterval(function () {
                        self.events.emit( 'agent::management::keepalive' );
                    }, self.options.keepAliveInterval);
                    cb();
                },
                function setState(cb) {
                    var funcs = [];
                    self.on('change_state', function (data = {}) {
                        self.logger.info("AGENT Action: " + data.action + " from: " + data.previous + " to: " + data.current + " data: " + JSON.stringify(data.data));
                    });
                    self.options.initialState.forEach( function (state) {
                        funcs.push(function (cbc){
                            self.once(state[1], function (){
                                cbc()
                            });
                            self.doAction(state[0])                                
                        });
                    });
                    async.series(funcs, cb);
                }
            ],
            function (error) {
                if (error) {
                    callback && callback.call(ctx || self, error);
                    return;
                }
                callback && callback.call(ctx || this);
            }
        );
    },
    finish: function (callback, ctx) {
        //TODO: implement it
        callback & callback.call(ctx || this);
    }
};
