
"use strict";

const EventEmitter = require('events');

class MyEmitter extends EventEmitter { }

/**
 * Create a new finite state Machine, optionally specifying a context for callback functions to be called against.
 * 
 */
var FSMMachine = module.exports = function (context) {
    this.context = context;
    this.emitter = new MyEmitter();
    this._stateTransitions = {};
    this._stateTransitionsAny = {};
    this._defaultTransition = null;
    this._initialState = null;
    this._currentState = null;
};

FSMMachine.prototype = {
    /**
     * Add a transition for a state. If nextState is not provided state will be the next state.
     * 
     * @param action
     * @param state
     * @param nextState
     * @param callback
     * 
    */
    addTransition: function (action, state, nextState, callback) {
        if (!nextState) {
            nextState = state;
        }
        return this._stateTransitions[[action, state]] = [nextState, callback];
    },
    /**
     * Add multiple transitions for a state. If nextState is not provided state will be the next state.
     * 
     * @param action
     * @param state
     * @param nextState
     * @param callback
     * 
    */
    addTransitions: function (actions, state, nextState, callback) {
        var action, _i, _len, _results;
        if (!nextState) {
            nextState = state;
        }
        _results = [];
        for (_i = 0, _len = actions.length; _i < _len; _i++) {
            action = actions[_i];
            _results.push(this.addTransition(action, state, nextState, callback));
        }
        return _results;
    },
    /**
     * Add a default transition for a state. If nextState is not provided state will be the next state.
     * 
     * @param state
     * @param nextState
     * @param callback
     * 
    */
    addTransitionAny: function (state, nextState, callback) {
        if (!nextState) {
            nextState = state;
        }
        return this._stateTransitionsAny[state] = [nextState, callback];
    },
    /**
     * Set the default transition.
     * 
     * @param state
     * @param callback
     * 
    */
    setDefaultTransition: function (state, callback) {
        return this._defaultTransition = [state, callback];
    },
    /**
     * Return the transition for the given action and state.
     * 
     * @param action
     * @param state
     * 
    */
    getTransition: function (action, state) {
        if (this._stateTransitions[[action, state]]) {
            return this._stateTransitions[[action, state]];
        } else if (this._stateTransitionsAny[state]) {
            return this._stateTransitionsAny[state];
        } else if (this._defaultTransition) {
            return this._defaultTransition;
        }
        throw new Error("Transition is undefined: (" + action + ", " + state + ")");
    },
    /**
     * Return the current state of the machine.
     * 
     * 
    */
    getCurrentState: function () {
        return this._currentState;
    },
    /**
     * Set the initial state of the machine.
     * 
     * @param state
     * 
    */
    setInitialState: function (state) {
        this._initialState = state;
        if (!this._currentState) {
            return this.reset();
        }
    },
    /**
     * Reset the machine to its initial state.
     * 
    */
    reset: function () {
        return this._currentState = this._initialState;
    },
    /**
     * Process an action.
     * 
     * @param action
     * 
    */
    process: function (action, data) {
        var self = this,
            result;
        result = this.getTransition(action, this._currentState);
        if (result[1]) {
            result[1].call(this.context || (this.context = this), action, data, function (error, appData) {
                if (error) {
                    throw new Error("Transition not done due to: " + error);
                }
                var prevState =  self._currentState;
                self._currentState = result[0];
                self.emitter.emit('change_state', { action: action, previous: prevState, current: self._currentState, data: data })
                self.emitter.emit(result[0], { previous: prevState, current: self._currentState, data: data, appData: appData })
            });
        }
    },
    /**
     * subscribe to events. Events:
     *    - 'change_state': when state changes, as value an object with 'previous' state and 'current' state
     *    - state: when state changes, an event is emitted with state as topic and 'previous' state as value
     * 
     * @param event
     * @param callback
     * 
    */
    on: function (event, callback) {
        this.emitter.on(event, callback);
    },
    /**
     * Subscribe one time to events, when event is triggered, this listener is removed and then invoked.
     * Events:
     *    - 'change_state': when state changes, as value an object with 'previous' state and 'current' state
     *    - state: when state changes, an event is emitted with state as topic and 'previous' state as value
     * 
     * @param event
     * @param callback
     * 
    */
    once: function (event, callback) {
        this.emitter.once(event, callback);
    }

};
