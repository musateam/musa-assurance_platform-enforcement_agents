"use strict";

const EventEmitter2 = require('eventemitter2').EventEmitter2;

var EventsManager = module.exports = function () {
    this.emitter = new EventEmitter2(
        {
            wildcard: true,// set this to `true` to use wildcards. It defaults to `false`
            delimiter: '::',// the delimiter used to segment namespaces, defaults to `.`
            newListener: false,  // set this to `true` if you want to emit the newListener event. The default value is `true`
            maxListeners: 20, // the maximum amount of listeners that can be assigned to an event, default 10
            verboseMemoryLeak: false // show event name in memory leak message when more than maximum amount of listeners is assigned, default false
        }
    );
};
EventsManager.prototype = {
    emit: function () {
        this.emitter.emit.apply(this.emitter, arguments);
    },
    on: function () {
        this.emitter.on.apply(this.emitter, arguments);
    },
    onAny: function () {
        this.emitter.onAny.apply(this.emitter, arguments);
    }
}
