
'use strict';

const EventsManager = require('./events');

module.exports = function setup(options, imports, register, app) {
    var eventManagers = {};
    var i = 0;
    var events = new EventsManager();

    register(
        null, // Error
        {
            events: events,
            eventsFactory: {
                createEvents: function () {
                    var events;
                    events = new EventsManager();
                    eventManagers[i] = events;
                    return i++;
                },
                onAny: function () {
                    return eventManagers[events].onAny.bind(eventManagers[events], arguments);
                },
                on: function () {
                    return eventManagers[events].on.bind(eventManagers[events], arguments);
                },
                emit: function () {
                    return eventManagers[events].emit.bind(eventManagers[events], arguments);
                }
            }
        }
    );
};