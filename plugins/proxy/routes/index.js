var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'MUSA Proxy Home', description: 'Default page for the MUSA Agent Proxy Plugin' });
});

module.exports = router;
