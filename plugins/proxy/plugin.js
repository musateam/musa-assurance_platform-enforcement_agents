
'use strict';

const Proxy = require('./proxy');

module.exports = function setup(options, imports, register, app) {
    var proxys = {};
    var i = 0;
    /*var proxy = new Proxy();
    proxy.setLoggerService(imports.log);
    proxy.setEventsService(imports.events);
    proxy.setApp(app);
    proxy.setup(options);*/

    register(
        null, // Error
        {
            //proxy: proxy,
            proxyFactory: {
                createProxy: function (config={}) {
                    var proxy;
                    proxy = new Proxy();
                    proxy.setLoggerService(imports.log);
                    proxy.setEventsService(imports.events);
                    proxy.setApp(app);
                    proxy.setup(imports.util.deepMerge(options, config));
                    proxys[i] = proxy;
                    return i++;
                },
                getSettings: function (proxy) {
                    return proxys[proxy].getSettings.call(proxys[proxy]);
                },
                getRequestInfo: function (proxy, req, callback, ctx) {
                    return proxys[proxy].getRequestInfo.call(proxys[proxy], req, callback, ctx);
                },
                on: function (proxy, event, callback, ctx) {
                    return proxys[proxy].on.call(proxys[proxy], event, callback, ctx);
                },
                getApp: function (proxy) {
                    return proxys[proxy].getApp.call(proxys[proxy]);
                },
                addHeaders: function (proxy, headers) {
                    return proxys[proxy].addHeaders.call(proxys[proxy], headers);
                },
                setLoggerService: function (proxy, logger) {
                    return proxys[proxy].setLoggerService.call(proxys[proxy], logger);
                },
                setup: function (proxy, options = {}) {
                    return proxys[proxy].setup.call(proxys[proxy], options);
                },
                addService: function (proxy, service, callback, ctx) {
                    return proxys[proxy].addService.call(proxys[proxy], service, callback, ctx);
                },
                start: function (proxy, callback, ctx) {
                    return proxys[proxy].start.call(proxys[proxy], callback, ctx);
                },
                stop: function (proxy, callback, ctx) {
                    return proxys[proxy].stop.call(proxys[proxy], callback, ctx);
                }
            }
        }
    );
};