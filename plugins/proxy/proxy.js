/**
 * Created by urkizu on 7/27/16.
 *
 *
 */

"use strict";

const _ = require('underscore');
const url = require('url');
const fs = require('fs');
const path = require('path');
const https = require('https');
const http = require('http');
const cors = require('cors')
const express = require('express');
const httpProxy = require('http-proxy');

var APIGateway = module.exports = function APIGateway() {
    const self = this;
    this.defaults = {
        headers: [
            { "name": "provider", "value": "X-access-provider" },
            { "name": "token", "value": "X-access-token" }
        ]
    }
    this.app = express();
    this.app.use(cors())
    this.proxy = httpProxy.createProxyServer({ xfwd: true, ignorePath: true, secure: false });
    //restream parsed body before proxying
    this.proxy.on('proxyReq', function(proxyReq, req, res, options) {
        if(req.body) {
            let bodyData = JSON.stringify(req.body);
            // incase if content-type is application/x-www-form-urlencoded -> we need to change to application/json
            proxyReq.setHeader('Content-Type','application/json');
            proxyReq.setHeader('Content-Length', Buffer.byteLength(bodyData));
            // stream the content
            proxyReq.write(bodyData);
        }
    });
    this.providers = {};
    this.app.use('*', function (req, res, next) {
        setImmediate(function () {
            try {
                self.events.emit('proxy::request::log', self.getRequestInfo(req));
            } catch (error) {
                self.logger.error(error)
            }
        });
        next()
    });
    this.app.use(require('morgan')('combined'));
    // view engine setup
    this.app.set('views', path.join(__dirname, 'views'));
    this.app.set('view engine', 'jade');
    // Headers
    this.headers = {};
    // Settings
    this.settings = {};
};

APIGateway.prototype = {
    getRequestInfo: function (req) {
        return {
            method: req.method,
            protocol: req.protocol,
            hostname: req.hostname,
            url: req.originalUrl,
            headers: JSON.stringify(req.headers),
            origin: req.origin || req.headers.referer,
            ip: req.ip
        }
    },
    generateErrorResponse: function (req, errorMsg) {
        var response;

        response = {};
        response["error"] = "Unexpected error found";

        if (_.isObject(errorMsg.error)) {
            response["error"] = errorMsg.error.message;
        } else if (errorMsg.msg) {
            response["error"] = errorMsg.msg;
        }
        if (errorMsg.extra) {
            response["extra"] = errorMsg.extra;
        }
        return response;
    },
    setApp: function (app){
        this.appCore = app;
        this.appCore.on('start', this.start.bind(this));
        this.appCore.on('update', this.update.bind(this));
        this.appCore.on('stop', this.stop.bind(this));
    },
    getApp: function () {
        return this.app;
    },
    getSettings: function (){
        return this.settings;
    },
    setLoggerService: function (logger) {
        this.logger = logger;
    },
    setEventsService: function (events) {
        this.events = events;
    },
    addHeaders: function (headers) {
        var self = this;

        (headers || []).forEach(function (header) {
            self.headers[header.name] = header.value;
        });
    },
    setup: function (options = {}) {
        if (_.isEmpty(options)) {
            return;
        }
        // name
        this.name = options.name || this.name;
        // listen host
        options.host && (this.settings.host = options.host);
        // listen port
        options.port && (this.settings.port = options.port);
        // remote
        this.settings.remote = options.remote || options.url || this.settings.remote;
        // headers
        this.addHeaders(options.headers || this.defaults.headers);
        // secure
        this.settings.secure = options.secure;
        this.settings.key = options.key;
        this.settings.cert = options.cert;
    },
    addService: function (service, callback, ctx) {
        var self = this;

        //TODO: Añadir routas para enrutar las llamadas de los servicios
        // Registrar las rutas en express
        // Crear un cliente en Auth0 y registrar en el las callbacks
        this.app[service.method.toLowerCase()](service.url, function (req, res, next) {
            var headers;

            headers = {};
            Object.keys(self.headers).forEach(function (header) {
                headers[header] = req.header(self.headers[header]);
            });

            callback && callback.call(ctx || this, undefined, headers, req, res, next);
        });
    },
    removeService: function (service, callback, ctx) {
        this.app[service.method.toLowerCase()](service.url, function (req, res, next) {
            next();
        });
    },
    start: function ( options={}, callback, ctx) {
        var self = this;

        if( this.settings.secure ) {
            this.httpServer = https.createServer(
                {
                    key: fs.readFileSync(path.join(__dirname, this.settings.key), 'utf8'),
                    cert: fs.readFileSync(path.join(__dirname, this.settings.cert), 'utf8')
                },
                this.app
            );
        } else {
            this.httpServer = http.createServer( this.app );            
        } 
        this.app.use(function (err, req, res, next) {
            if (err) {
                const errorResponse = self.generateErrorResponse(req, err);
                const code = err.code || 401;
                self.events.emit("proxy::request::error", Object.assign(self.getRequestInfo(req), { code: code, errorResponse }));
                res.status(code).send(errorResponse);
                return;
            }
            next();
        });
        // MUSA Gateway public pages
        this.app.use('/', require('./routes/index'));
        // Route to target 
        this.app.use('/*', function (req, res, next) {
            self.proxy.web(
                req,
                res,
                {
                    target: self.settings.remote + req.originalUrl,
                    rejectUnauthorized: false
                },
                function (error) {
                    // TODO: Generate an error page
                    if (error) {
                        res.status(403).send(error);
                    }
                    self.events.emit("proxy::request::proxied", Object.assign(self.getRequestInfo(req), { target: self.settings.remote + req.originalUrl }));
                }
            );
        });
        this.httpServer.listen(this.settings.port || 3000);
        console.log("Proxy Listening in http://" + (this.settings.host || '0.0.0.0') + ":" + (this.settings.port || 3000));
        this.httpServer.on('listening', function (error) { callback && callback.call(ctx || this, error) });
    },
    update: function (settings={}, callback,ctx){
        if( !settings[this.name] ){
            callback && callback.call(ctx || this);
            return;            
        }

        var psettings;
        var remote =

        psettings = settings[this.name].options || {};
        // Only allow to update this properties, problably will be better to call setup 
        this.settings.remote = settings[this.name].remote || settings[this.name].url || psettings.remote || psettings.url || this.settings.remote;
        psettings.host && (this.settings.host = psettings.host);
        psettings.port && (this.settings.port = psettings.port);
        this.addHeaders(psettings.headers);
        callback && callback.call(ctx || this);
    },
    stop: function (options= {}, callback, ctx) {
        this.httpServer.close(function (error) { callback && callback.call(ctx || this, error) });
    }
};
