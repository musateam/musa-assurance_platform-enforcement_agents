var express = require('express');
var router = express.Router();
var jsonwebtoken = undefined;

/* POST token verify. */
router.post('/verify', function (req, res, next) {
    var token,
        options;

    try {
        token = req.body['token'];
        options = JSON.parse(req.body['options'] || "{}" );

        if( !token ) {
            res.status(403).send({ error: "No token found on 'token' body" });
            return;
        }

        jsonwebtoken.verify(options.provider, token, options, function (error, decoded) {
            if (error) {
                res.status(403).send({ error: error });
                return;
            }

            res.status(200).send({ result: decoded });
        });

    } catch (err) {
        res.status(403).send({ error: err });
    }
});

/* POST token to sign. */
router.post('/sign', function (req, res, next) {
    var payload,
        options;

    try {
        payload = req.body['payload'];
        options = req.body['options'] || "{}";

        if(!payload){
            res.status(403).send({ error: "No token found on 'token' body" });
            return;            
        }

        jsonwebtoken.sign(payload, options, function (error, token) {
            if (error) {
                res.status(403).send({ error: error });
                return;
            }

            res.status(200).send({ result: token });
        });

    } catch (err) {
        res.status(403).send({ error: err });
    }
});

router.setJWT = function (jwt) {
    jsonwebtoken = jwt;
};

module.exports = router;
