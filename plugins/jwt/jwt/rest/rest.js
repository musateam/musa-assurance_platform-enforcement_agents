const _ = require('underscore');
const express = require('express');
const services = require('./services');


var REST = module.exports = function REST( ) {
    this.defaults = {
        provider: 'auth0',
        serviceName: 'jwt'        
    }
};

REST.prototype = {
    setup: function (options={}){
        this.settings = _.defaults( options, this.defaults );
        // Register operations
        this.settings.app.use("/" + this.settings.provider + '/' + this.settings.serviceName, services);
    },
    setJWT: function (jwt) {
        return services.setJWT(jwt);
    }
};