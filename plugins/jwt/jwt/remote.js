/**
 * Created by urkizu on 8/3/16.
 */

const request = require('request');
const url = require('url');
const jwt = require('jsonwebtoken');

var JWT = module.exports = function JWT(manager) {
    this.manager = manager;
    this.remote = undefined;
};

JWT.prototype = {
    setup: function (options = {}) {
        this.remote = options.remote;
    },
    decode: function (token) {
        return jwt.decode(token, { complete: true });
    },
    verify: function (provider, token, options, callback, ctx) {
        var res,
            headers = {},
            self = this;

        headers[this.remote.headers.token] = token;
        headers[this.remote.headers.options] = JSON.stringify(options);

        request.post(
            {
                strictSSL: false,
                url: new Buffer(provider, 'utf8') || this.remote.host + this.remote.verify,
                headers: headers
            },
            function (error, response, body) {
                if (error) {
                    callback && callback.call(ctx || this, error);
                    return;
                }
                try {
                    res = JSON.parse(body);
                } catch (err) {
                    callback && callback.call(ctx || this, err);
                    return;
                }
                callback && callback.call(ctx || this, undefined, self.decode(token).payload);
            }
        );
    },
    sign: function (provider, payload, options, callback, ctx) {
        var res,
            headers = {},
            self = this;

        headers[this.remote.headers.payload] = payload;
        headers[this.remote.headers.options] = JSON.stringify(options);

        request.post(
            {
                url: new Buffer(provider, 'utf8') || this.remote.host + this.remote.sign,
                headers: headers
            },
            function (error, response, body) {
                if (error) {
                    callback && callback.call(ctx || this, error);
                    return;
                }

                if (res.error) {
                    callback && callback.call(ctx || this, res.error);
                    return;
                }

                try {
                    res = JSON.parse(body);
                } catch (err) {
                    callback && callback.call(ctx || this, err);
                    return;
                }
                if (!res.result) {
                    callback && callback.call(ctx || this, new Error("Invalid token."));
                    return;
                }
                callback && callback.call(ctx || this, undefined, res);
            }
        );
    }
};