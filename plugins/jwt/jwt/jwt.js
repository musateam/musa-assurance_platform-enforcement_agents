/**
 * Created by urkizu on 8/3/16.*/
var _ = require('underscore'),
    path = require('path'),
    fs = require('fs'),
    async = require('async'),
    jwt = require('jsonwebtoken');

var JWT = module.exports = function JWT(manager) {
    this.manager = manager;
    this.pem = undefined;
    this.cert = undefined;
};

JWT.prototype = {
    setup: function (options) {
        if(options.secret) {
            this.pem = options.secret;  // get public key
            this.cert = options.secret;  // get cert
            this.algorithms = 'HS256';
        } else {
            this.pem = fs.readFileSync(path.resolve(__dirname, '../..', options.pem));  // get public key
            this.cert = fs.readFileSync(path.resolve(__dirname, '../..', options.cert));  // get cert
            this.algorithms = 'RS256';
        }
        this.claims = {};
        this.claims['iss'] = options.issuer || "musa";
        this.claims['exp'] = options.expiresIn || 60 * 60;
        this.claims['nbf'] = options.notBefore || 1;
    },
    decode: function (token) {
        return jwt.decode(token, { complete: true });
    },
    verify: function (provider, token, options, callback, ctx) {
        var self = this,
            verifyOptions;

        if (_.isEmpty(token)) {
            callback && callback.call(ctx || this, new Error('Unable to verify an empty  token.'));
            return;
        }

        if (_.isEmpty(options)) {
            options = {};
        }

        verifyOptions = {};

        verifyOptions.issuer = this.claims["iss"];
        verifyOptions.algorithms = [this.algorithms];
        options.audience && (verifyOptions.audience = options.audience);
        options.subject && (verifyOptions.subject = options.subject);

        jwt.verify(token, self.pem, verifyOptions, function( error, results) {
            if (error) {
                callback && callback.call(ctx || this, error);
                return;
            }
            callback && callback.call(ctx || this, undefined, results);
        });
    },
    sign: function (payload = {}, options={}, callback, ctx) {
        let signOptions;

        try {
            signOptions = {};
            payload.exp = options.expiresIn || Math.floor(Date.now() / 1000) + (this.claims['exp']);
            payload.nbf = options.notBefore || Math.floor(Date.now() / 1000) + (this.claims['nbf']);
            payload.iss = this.claims['iss'];

            options.audience && ( payload.aud = options.audience );
            options.subject && ( payload.sub = options.subject );
            options.jwtid && ( payload.jti = options.jwtid );

            return jwt.sign(payload, options.cert || this.cert, Object.create( { algorithm: this.algorithms }, options ), callback);
        } catch( err ) {
            callback && callback.call( ctx || this, err );
        }
    }
};