
'use strict';

const Jwt = require('./jwt');

module.exports = function setup(options, imports, register, app) {
    var jwts = {};
    var i = 0;
    /*var jwt = new Jwt();
    jwt.setLoggerService(imports.log);
    jwt.setEventsService(imports.events);
    jwt.setProxyFactoryService(imports.proxyFactory);
    jwt.setApp(app);
    jwt.setup(options);
    jwt.start();*/

    register(
        null, // Error
        {
            //jwt: jwt,
            jwtFactory: {
                createJwt: function (config={}) {
                    var jwt;
                    jwt = new Jwt();
                    jwt.setLoggerService(imports.log);
                    jwt.setEventsService(imports.events);
                    jwt.setProxyFactoryService(imports.proxyFactory);
                    jwt.setApp(app);
                    jwt.setup(imports.util.deepMerge(options, config));
                    jwts[i] = jwt;
                    return i++;
                },
                decode: function (jwt, token) {
                    return jwts[jwt].decode.call(jwts[jwt]);
                },
                verify: function (jwt, provider, token, options, callback, ctx) {
                    return jwts[jwt].verify.call(jwts[jwt], provider, token, options, callback, ctx);
                },
                sign: function (jwt, payload, options={}, callback, ctx) {
                    return jwts[jwt].sign.call(jwts[jwt], payload, options, callback, ctx);
                },                
                start: function (jwt, callback, ctx) {
                    return jwts[jwt].start.call(jwts[jwt], callback, ctx);
                },
                stop: function (jwt, callback, ctx) {
                    return jwts[jwt].stop.call(jwts[jwt], callback, ctx);
                }
            }
        }
    );
};