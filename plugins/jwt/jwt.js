/**
 * Created by urkizu on 7/27/16.
 *
 *
 */

"use strict";

const _ = require('underscore');
const Remote = require('./jwt/remote');
const Jwt = require('./jwt/jwt');
const Rest = require('./jwt/rest/rest');

var JWT = module.exports = function JWT() {
    this.defaults = {
        'mode': 'direct', // available direct||rest
        'rest': undefined,
        'pem': './certs/.pem',
        'cert': './certs/musa.cert',
        'issuer': 'https://musa.eu.auth0.com/',
        'remote': {
            'host': 'https://musa.eu/jwt',
            'headers': {
                'payload': 'X-access-payload',
                'token': 'X-access-token',
                'options': 'X-token-options'
            },
            'verify': '/verify',
            'sign': '/sign'
        }
    }
    this.services = {};
    this.remote = new Remote(this);
    this.jwt = new Jwt(this);
    this.rest = undefined;
};

JWT.prototype = {
    setup: function (options = {}) {
        if (_.isEmpty(options)) {
            return;
        }
        this.name = options.name;
        this.settings = _.defaults(options, this.defaults);
        this.remote.setup(this.settings.remote);
        this.jwt.setup(this.settings);
        switch(this.settings.mode){
            case 'remote':
                this.current = this.remote;
                break;
            case 'direct':
            default:
                this.current = this.jwt;
                break;                
        }
        if( options.rest ){
            this.addRESTInterface(options.rest)
        }
    },
    addRESTInterface: function (options){
        this.rest = new Rest();
        this.rest.setJWT(this.jwt);
        if( !options.app ){
            this.proxy = this.proxyFactory.createProxy();
            this.proxyFactory.getApp(this.proxy).use(require('body-parser').json());
            this.proxyFactory.addService(this.proxy, { method: 'GET', url: '/' }, function( error, headers, req, res, next ) {
                res.render('index', { title: 'MUSA JWT Server', description: 'Default page for JWT Server' });
            });
            options.app = this.proxyFactory.getApp(this.proxy);
        }
        this.rest.setup(options);
    },
    setApp: function (app){
        this.app = app;
    },
    setLoggerService: function (logger) {
        const self = this;

        this.logger = logger;
        this.logger.on( 'info', function( data) { self.events.emit( 'log::jwt::info', { msg: data })});
        this.logger.on( 'warn', function( data) { self.events.emit( 'log::jwt::warn', { msg: data })});
        this.logger.on( 'cerror', function( data) { self.events.emit( 'log::jwt::error', { msg: data })});
    },
    setEventsService: function (events) {
        this.events = events;
    },
    setProxyFactoryService: function (proxyFactory) {
        this.proxyFactory = proxyFactory;
    },
    decode: function(token){
        return this.current.decode(token, { complete: true });
    },
    verify: function (provider, token, options, callback, ctx) {
        return this.current.verify(provider, token, options, callback, ctx);
    },
    sign: function (provider, jwt, callback, ctx) {
        return this.current.sign(provider,token, options, callback, ctx);
    },
    start: function ( options={}, callback, ctx) {
        this.logger.info("JWT start");
        if( this.hasOwnProperty( 'proxy' ) ) {
            this.proxyFactory.start(this.proxy, options, callback);
            return;
        }
        callback && callback.call( ctx || this);
    },
    update: function (settings, callback, ctx){
        var pSettings;

        pSettings = settings[this.name];
        // Only update controlled parameters
        this.remote.setup(pSettings.remote);
        this.jwt.setup(pSettings);
        switch(pSettings.mode){
            case 'remote':
                this.current = this.remote;
                break;
            case 'direct':
            default:
                this.current = this.jwt;
                break;                
        }
        if( pSettings.rest ){
            this.addRESTInterface(options.rest)
        }
        this.app.setSettings(this.name, pSettings);
        callback && callback.call(ctx || this);
    },
    stop: function (callback, ctx) {
        if( this.hasOwnProperty( 'proxy' ) ) {
            this.proxyFactory.stop(this.proxy, options, callback);
            return;
        }
        callback && callback.call( ctx || this);
    }
};
