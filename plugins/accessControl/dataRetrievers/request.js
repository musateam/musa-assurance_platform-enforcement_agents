'use strict';

const Utils = require('./utils');


exports = module.exports = function (source, key, context, callback) {

    if (!context) {
        // Return nothing
        return callback();
    }

    // Check allowed keys
    if (['method', 'path'].indexOf(key) === -1) {
        return callback();
    }

    callback(null, Utils.reach(context, key));
};
