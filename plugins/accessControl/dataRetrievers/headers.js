'use strict';

const Utils = require('./utils');

exports = module.exports = function (source, key, context, callback) {

    if (!context) {
        // Return nothing
        return callback();
    }
    
    callback(null, Utils.reach(context, 'headers.' + key));
};
