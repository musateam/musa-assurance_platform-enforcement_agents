'use strict';

const Utils = require('./utils');

exports = module.exports = function (source, key, context, callback) {

    if (!context) {
        // Return nothing
        return callback();
    }

    context.info = {
        ip: context.ip,
        host: context.host,        
        hostname: context.hostname,        
        protocol: context.protocol,        
    };
    
    callback(null, Utils.reach(context, 'info.' + key));
};
