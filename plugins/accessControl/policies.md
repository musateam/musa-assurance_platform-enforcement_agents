
  - [Terms](#terms)
  - [Setting up a policy](#setting-up-a-policy)
      - [Target matching](#target-matching)
          - [AND](#and)
          - [OR](#or)
      - [Policy and Rules combinatory algorithms](#policy-and-rules-combinatory-algorithms)
      - [Rule effects](#rule-effects)
      - [Rule](#rule)
      - [Policy](#policy)
      - [Policy Set](#policy-set)
  - [Configuration](#configuration)
      - [Global Policy](#global-policy)
      - [Service Policy](#service-policy)
      - [File/Dir Policy](#filedir-policy)
      - [Combinning Policies](#combinning-policies)
      - [Data retrievers](#data-retrievers)

## Terms
-----------------------
* `Target` - A set of key-value pairs which are matched with the available information on a request. It is used to decide if a *Rule*, *Policy* or *Policy Set* apply to the request's case.
* `Rule` - A *Rule* specifies if a matched *Target* should have or not access to the route.
* `Policy` - A *Policy* is composed by a set of *Rules*. It specifies how the combination of the *Rules'* results should be considered.
* `Policy Set` - A *Policy Set* is composed by a set of *Policies*. It also specifies how the combination of the *Policies'* results should be considered.

## Setting up a policy
-----------------------
### Target matching
Targets are the conditions which will define if a policy set, policy or rule apply in a request. 
If the policy set, policy or rule should always apply, you can simply omit the `target`.

When present, it can either be a target element or an array of target elements.

When the array has more than one target element, they are combined with an `OR` condition.
All the keys inside a target element are combined with an `AND` condition.

Check the following examples:

#### AND

```js
{
  'credentials:rol': 'developer',
  'credentials:premium': true
}
```

With this target, only users with rol `developer` **and** with `premium` account will match.

So, if the logged in user has the following information:

```js
  id: 'user00001',
  rol: ['developer'], // match
  premium: true, // match
  ...
```

Then, the *rule* or *policy* with the configured *target* will be evaluated, because the target applies.

But, if the logged in user has one of the following information:

```js
{
  id: 'user00002',
  rol: ['developer'], // match
  premium: false, // do not match :-(
  ...
}
```

```js
{
  id: 'user00003',
  rol: ['admin'], // do not match :-(
  premium: true, // match
  ...
}
```

Then, the rule or policy with the configured target will not be evaluated.
Since the match used is `AND`, the user doesn't match the target.

#### OR

```
[
  {
    'credentials:id': 'developer'
  },
  {
    'credentials:premium': true
  },
  {
    'credentials:id': 'user00002'
  }
]
```

With this target, any user in the group `developer` **or** with `premium` account **or** with username `user00002` will be matched.

So, users with the following information will be matched:

```js
{
  id: 'user00001',
  rol: ['developer'], // match
  premium: false,
  ...
}
```

```js
{
  id: 'user00002', // match
  rol: ['reader'],
  premium: false,
  ...
}
```

```js
{
  id: 'user00003',
  rol: ['reader'],
  premium: true, // match
  ...
}
```

```js
{
  id: 'user00004',
  rol: ['writer'], // match
  premium: true, // match
  ...
}
```

But, not the one with the following document:

```js
{
  id: 'user00005',
  rol: ['reader'],
  premium: false,
  ...
}
```

The following words are prefixes that can be used for matching information:

* `credentials` - Information from jwt token object. Information in this object depends on your authentication implementation.
* `connection` - Connection information
  * `connection:ip` - Content of the remote IP address. The value of this property is derived from the left-most entry in the X-Forwarded-For header. This header can be set by the client or by the proxy.
  * `connection:host` - Content of the HTTP 'Host' header (e.g. 'example.com:8080').
  * `connection:hostname` - The hostname part of the 'Host' header (e.g. 'example.com').
  * `connection:protocol` - Contains the request protocol string: either http or (for TLS requests) https.
* `headers` - Any HTTP header received on request (e.g 'accept-encoding').
* `query` - Query parameters, as in `request.query`.
* `param` - URL parameters, as in `request.params`.
* `request` - Other request information:
  * `request:path` - Requested path.
  * `request.method` - Requested method (e.g. `post`).


### Policy and Rules combinatory algorithms

When there is more than one policy inside a policy set or more than one rule inside a policy,
the combinatory algorithm will decide the final result from the multiple results.

There are, at the moment, two possibilities:

* `permit-overrides` - If at least one policy/rule permits, then the final decision
     for that policy set/policy should be `PERMIT` (deny, unless one permits)
* `deny-overrides` - If at least one policy/rule denies, then the final decision
     for that policy set/policy should be `DENY` (permit, unless one denies)

### Rule effects

If a rule applies (target match), the `effect` is the access decision for that rule. It can be:

* `permit` - If rule apply, decision is to allow access
* `deny` - If rule apply, decision is to deny access

When a policy set, policy or rule do not apply (the target don't match), then the decision is `undetermined`.
If all the policy sets, policies and rules have the `undetermined` result, then the access is denied,
 since it is not clear if the user can access or not the route.


### Rule

A __Rule__ defines a decision to _allow_ or _deny_ access. It contains:

* `target` (optional) - The target (default: matches with any)
* `effect` - The decision if the target matches. Can be `permit` or `deny`

Example

```
{
  target: {'credentials:blocked': true}, // if the user is blocked
  effect: 'deny'  // then deny
}
```


### Policy

A __Policy__ is a _set of rules_. It contains:

* `target` (*optional*) - The target (*default*: matches with any)
* `apply` - The combinatory algorithm for the rules
* `rules` - An array of rules

Example

```js
{
  // if writer AND premium account
  target: {
    'credentials:group': 'writer',
    'credentials:premium': true
  },
  apply: 'deny-overrides', // permit, unless one denies
  rules: [
    {
      target: { 'credentials:username': 'bad_user' }, // if the username is bad_user
      effect: 'deny'  // then deny
    },
    {
      target: { 'credentials:blocked': true }, // if the user is blocked
      effect: 'deny'  // then deny
    },
    {
      effect: 'permit' // else permit
    }
  ]
}
```


### Policy Set

A __Policy Set__ is a set of __Policies__. It contains:

* `target` (_optional_) - The target (_default_: matches with any)
* `apply` - The combinatory algorithm for the policies
* `policies` - An array of policies

Example

```js
{
  target: [{ 'credentials:group': 'writer' }, { 'credentials:group': 'publisher'}], // writer OR publisher
  apply: 'permit-overrides', // deny, unless one permits
  policies: [
    {
      target: { 'credentials:group': 'writer', 'credentials:premium': true }, // if writer AND premium account
      apply: 'deny-overrides', // permit, unless one denies
      rules: [
        {
          target: { 'credentials:username': 'bad_user'}, // if the username is bad_user
          effect: 'deny'  // then deny
        },
        {
          target: { 'credentials:blocked': true }, // if the user is blocked
          effect: 'deny'  // then deny
        },
        {
          effect: 'permit' // else permit
        }
      ]
    },
    {
      target: { 'credentials:premium': false }, // if (writer OR publisher) AND no premium account
      apply: 'permit-overrides', // deny, unless one permits
      rules: [
        {
          target: { 'credentials:username': 'special_user' }, // if the username is special_user
          effect: 'permit'  // then permit
        },
        {
          effect: 'deny' // else deny
        }
      ]
    }
  ]
}
```

## Configuration
-----------------------

### Global Policy

If you wish to define a default access control policy for the routes, you can do it with `policies` key inside the `root` config file.

```javascrit
{
    "enforcement": {
        "enable": true,
        "apply": "deny-overrides",
        "dataRetrievers": [],
        "policies": [
            {
              target: { 'credentials:group': 'readers' },
              apply: 'deny-overrides', // Combinatory algorithm
              rules: [
                {
                  target: { 'credentials:username': 'bad_guy' },
                  effect: 'deny'
                },
                {
                  effect: 'permit'
                }
              ]
            }
        ]
    },
  ...
});
```

This configuration will allow access to all the routes to all the users in the `readers` group, except to the user `bad_guy`.

### Service Policy

If you wish to define access control policies for a single service, you can do it at the service level configuration:

```javascript
{
    ...
    "services": [
        {
            "id": 1,
            "method": "GET",
            "url": "/service1/",
            "remote": "http://backend-musa.enforcement.cu.cc:3002/service1/",
            "enforcement": {
                "enable": true,
                "apply": "deny-overrides",
                "policies": [
                      {
                      target: { 'credentials:group': 'readers' },
                      apply: 'deny-overrides', // Combinatory algorithm
                      rules: [
                        {
                          target: { 'credentials:username': 'bad_guy' },
                          effect: 'deny'
                        },
                        {
                          effect: 'permit'
                        }
                      ]
                    }
                ]
            }
        }
    ]
    ...
}
```

If you have access control policies configured globally and at app level, this configuration is added to them.


### File/Dir Policy

It is also possible to retrieve the from a file or a directory, instead of defining them directly

```javascript
{
    "enforcement": {
        "enable": true,
        "apply": "deny-overrides",
        "policies": [
            "./policies/"
        ]
    }
}
```

### Combinning Policies

In this example, it is assumed that your policies have a `resource` key with `path` and `method` sub-keys.

```js
{
  resource: { // resource identifies what is being requested
    path: '/example',
    method: 'get'
  },
  target: { 'credentials:group': 'readers' },
  apply: 'deny-overrides', // Combinatory algorithm
  rules: [
    {
      target: { 'credentials:username': 'bad_guy' },
      effect: 'deny'
    },
    {
      effect: 'permit'
    }
  ]
}
```

### Data retrievers

You can define your own data sources for target matching. To do so, you can define ac custom dataRetriever.

#TODO: Add info how to extend dataRetreivers

```js
    dataRetrievers: [
        {
            handles: ['document'], // Name the source this data retriever handles
            handler: (source, key, context, callback) => {

                // You can use the key as you wish
                // e.g. key: 12345.name
                const splitKey = key.split('.');

                const id = splitKey[0];
                const field = splitKey[1];

                const query = {
                    _id: id,
                    // The context is the Request object
                    user: Hoek.reach(context, 'auth.credentials._id')
                };

                db.collection('documents').findOne(query, (err, result) => {

                    if (err) {
                        return callback(err);
                    }

                    // Pass the value to the callback
                    callback(null, Hoek.reach(result, field));
                });
            }
        }
    ]
  }
}, function(err) {
  ...
});
```

Then, you can use it in your targets:

```js
{
    target: { 'document:12345.title': 'The Swallow\'s Tale' },
    ...
}
```
