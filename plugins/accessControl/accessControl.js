#!/usr/bin/env node

/**
 * Created by urkizu on 7/29/16.
 */
const url = require('url');
const _ = require('underscore');
const Enforcement = require('./enforcement');

var AccessControl = module.exports = function AccessControl() {
    var self = this;

    this.settings = {};
    this.services = {};
    this.events = undefined;
    this.logger = undefined;
    this.agent = undefined;
    this.proxy = undefined;
    // Enforcement registered
    this.enforcement = new Enforcement();
    this.defaults = {
        enforcement: {
            "enable": true,
            "apply": "permit-overrides",
            "policies": [ ]
        }
    }
    this.handleError = function (code = 403, req, msg = 'unexpected error', result = 'None') {
        try { 
            self.events.emit(
                'ac::request::error',
                Object.assign(
                    self.proxy.getRequestInfo(self.services['proxy'], req),
                    { code: code, msg: msg },
                    { result: result }
                )
            );
        } catch ( error ) {
            self.logger.error(  error + "emitting error." );
        }
        return { code: code, error: new Error(msg)};
    };
    this.enforcementCallback = function (error, headers, req, res, done) {
        if (!self.enable) {
            self.events.emit("ac::request::pass", Object.assign(self.proxy.getRequestInfo(self.services['proxy'], req), { msg: 'Allowed to consume request', result: 'pass' }));
            done();
            return;
        }

        // TODO: Use Authorization header
        if (_.isEmpty(headers['token'])) {
            done(self.handleError(403, req, "Unable to verify an empty provider please add a header with X-access-token and value with a token."));
            return;
        }
        
        self.jwtFactory.verify(self.jwt, headers['provider'], headers['token'], {}, function (error, decoded) {
            if (error) {
                done(self.handleError( 403, req, error));
                return;
            }

            req.token = decoded;

            self.enforcement.allow(1, req, function (error, result) {
                if (error) {
                    done(self.handleError(403, req, error));
                    return;
                }
                try {
                    switch (result) {
                        case self.enforcement.results.deny:
                            done( self.handleError(self.enforcement.defaults.responseCode.onDeny, req, new Error('No permissions to access this resource'), 'deny'));
                            break;
                        case self.enforcement.results.undetermined:
                            done( self.handleError(self.enforcement.defaults.responseCode.onUndetermined, req, new Error('Could not evaluate access rights to resource'), 'undetermined'));
                            break;
                        default:
                            self.events.emit("ac::request::pass", Object.assign(self.proxy.getRequestInfo(self.services['proxy'], req), { msg: 'Allowed to consume request', result: 'pass' }));
                            done();
                            break;
                    }
                } catch (error) {
                    self.logger.error(error + " found processing enforcement response");
                    done({ code: 403, msg: error });
                }
            })
        })
    };
};

AccessControl.prototype = {

    setup: function (settings = {}) {
        var agent,
            services,
            self = this;

        this.logger.info("Access control setup");
        // name
        this.name = settings.name || this.name;
        // Store settings
        this.settings = JSON.parse(JSON.stringify(settings));
        // Enable or disable
        this.enable = this.settings.enable;
        this.enforcement.setEnable(this.settings.enable);
        // Setup Global Enforcement
        this.enforcement.setup(this.settings.enforcement || this.defaults.enforcement);
        // KLUDGE: Create MUSA services from config. Not applying policies from here
        this.enforcement.addService(
            {
                id: 1,
                enforcement: this.defaults.enforcement
            }
        );
    },
    setJwtService: function (jwtFactory) {
        this.jwtFactory = jwtFactory;
        this.jwt = jwtFactory.createJwt();
    },
    setApp: function ( app ){
        const self = this;
        this.app = app;
        this.app.on('start', this.start.bind(this));
        this.app.on('update', this.update.bind(this));
        this.app.on('stop', this.stop.bind(this));
    },
    setAgentFactoryService: function (agentFactory) {
        this.agent = agentFactory;
        this.services['agent'] = this.agent.createAgent( );
        this.agent.setLoggerService(this.services['agent'], this.logger);
    },
    setProxyFactoryService: function (proxyFactory) {
        this.proxy = proxyFactory;
        this.services['proxy'] = this.proxy.createProxy();
        this.proxy.addService(this.services['proxy'], { method: 'GET', url: '/' }, function( error, headers, req, res, next ) {
            res.render('index', { title: 'MUSA Access Control Server', description: 'Default page for access control server' });
        });
    },
    setEventsService: function (events) {
        this.events = events;
        this.enforcement.setEvents(this.events);
    },
    setLoggerService: function (logger) {
        const self = this;

        this.logger = logger;
        this.enforcement.setLogger(this.logger);
        this.logger.on( 'info', function( data) { self.events.emit( 'log::ac::info', { msg: data })});        
        this.logger.on( 'warn', function( data) { self.events.emit( 'log::ac::warn', { msg: data })});
        this.logger.on( 'cerror', function( data) { self.events.emit( 'log::ac::error', { msg: data })});
    },
    init: function (callback, ctx) {
        var self = this;

        this.agent.init(this.services['agent'], function (error) {
            if (error) {
                callback && callback.call(ctx || self, error);
                return;
            }

            self.proxy.addService(self.services['proxy'], { method: 'all', url: '/*' }, self.enforcementCallback);
            callback && callback.call(ctx || self);
        });
    },
    finish: function (callback, ctx) {
        var self = this;

        this.agent.finish(this.services['agent'], function (error) {
            if (error) {
                callback && callback.call(ctx || self, error);
                return;
            }

            self.proxy.removeService(self.services['proxy'], { method: 'all', url: '/*' });
            callback && callback.call(ctx || self);
        });

    },
    start: function (settings = {}, callback) {
        const self = this;
        this.logger.info("Access control start");
        this.started = true;
        callback && callback();
    },
    update: function(settings = {}, callback){
        if (settings[this.name]) {
            settings[this.name].options.enforcement.reload = true;
            this.setup(settings[this.name].options);
        }
        // TODO: Update the info
        callback && callback();
    },
    stop: function (settings = {}, callback) {
        this.logger.info("Access control stop");
        callback && callback();
    }
}





