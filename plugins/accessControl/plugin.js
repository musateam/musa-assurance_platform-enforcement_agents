
'use strict';

const AccessControl = require('./accessControl');

module.exports = function setup(options, imports, register, app) {
    const ac = new AccessControl();
    ac.setLoggerService(imports.log);
    ac.setEventsService(imports.events);
    ac.setJwtService(imports.jwtFactory);
    ac.setAgentFactoryService(imports.agentFactory);
    ac.setProxyFactoryService(imports.proxyFactory);
    ac.setApp(app);
    ac.setup(options);
    ac.init(function (error) {
        register(error, {});
    });
};