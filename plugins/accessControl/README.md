<!-- TOC -->

- [Plugin AccessControl](#plugin-accesscontrol)
    - [Configuration](#configuration)
    - [Policies](#policies)
    - [Service](#service)
    - [Issue Reporting](#issue-reporting)
    - [Author](#author)
    - [License](#license)

<!-- /TOC -->
# Plugin AccessControl
A reverse proxy that intercepts all request to target and ensure that the user have the rigths to reach/consume target. It includes a rule engine based on XACML policies.

It's provides the following features: 

 - A **XACML PEP** that intercepts access request and ensure XACML PDP decission  
 - A **XACML PDP** to evaluate policies   
 - A **XACML PIP** to provide attributes, it's implemented as dataRetreivers that get info from request (origin, date, ip,...), user (email,name,...)
 - JWT client to verify JWT token

## Configuration
------------
These are the main parts of MUSA Access Control plugin configuration:

* **jwtissuer**: 
    - `host`: The url of the REST Host. Ex: "https://idm-musa.enforcement.cu.cc:3000"
    - `service`: REST service used to verify JWT tokens.
    - `method` :  
* **enforcement**:  enforcement config
    - `enabled` {boolean}: if disabled the enforcement is bypassed. Default: true
    - `apply` {string}: the default apply to take into account. Default: "deny-overrides"
    - `dataRetrievers` {array}: an array of data retreivers to use. There are a list of core dataRetreivers. See [policies and dataRetreiver ](./policies.md) for more info. Default: []
    - `policies` {array}: an array of the policies. Depending of the type of each element it will be loaded. Default: []
        - **string**: it's represent a relative path directory where policies expected to be. All files located on this directory ( and deeper ) will be loaded 
        - **json**: it's represent a policy and will be loaded as is 

## Policies
------------
The policies used by XACML PDP are based on XACML but instead of using XML it's format is JSON. To see more about it 
See [policies and dataRetreiver ](./policies.md)

## Service
------------
This service provides no API to be used by others services

## Issue Reporting
------------
If you have found a bug or if you have a feature request, please report them to admin email.

## Author
------------
[Borja Urkizu Tecnalia](mailto:borja.urquizu@tecnalia.com )

## License
------------