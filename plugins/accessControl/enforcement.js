/**
 * Created by urkizu on 8/7/16.
 */

const Schemas = require('./schemas');
const _ = require('underscore');
const fs = require('fs');
const path = require('path');
const RbacCore = require('rbac-core');
const DataRetrievalRouter = RbacCore.DataRetrievalRouter;

var Enforcement = module.exports = function Enforcement(  ) {
    var self = this;

    this.services = {};
    this.enable = true;
    this.logger = require('winston');
    this.defaults = {
        apply: "deny-overrides",
        onError: function (request, response, err, code) {
            response.status(code || 401).send({ error: err });
        },
        responseCode: {
            onDeny: 401,
            onUndetermined: 401
        },
        dataRetrievers: []
    };

    this.dataRetriever = undefined;

    this.recursiveReadDir = function (ppath) {
        var list = [],
            files = fs.readdirSync(ppath),
            stats;
            
        files.forEach(function (file) {
            stats = fs.lstatSync(path.join(ppath, file));
            if (stats.isDirectory()) {
                list = list.concat(self.recursiveReadDir(path.join(ppath, file)));
            } else {
                list.push(path.join(ppath, file));
            }
        });

        return list;
    };

    this.walkSync = function (path) {
        var stats;

        stats = fs.lstatSync(path);

        if (stats.isFile()) {
            return [path];
        }

        if (stats.isDirectory()) {
            return this.recursiveReadDir(path);
        }

        throw new Error(path + " not file or directory looking for policies.");
    };
    this.policies = [];
};

Enforcement.prototype = {
    results: {
        permit: RbacCore.PERMIT,
        deny: RbacCore.DENY,
        undetermined: RbacCore.UNDETERMINED
    },

    retrievePolicy: function (policy) {
        var files,
            ret = [];

        // If is an object just return it
        if (_.isObject(policy)) {
            return [policy];
        }

        if (_.isString(policy)) {
            files = this.walkSync(path.join(__dirname, policy));
            files.forEach(function (file) {
                ret.push(JSON.parse(fs.readFileSync(file, 'utf8')));
            });
        }

        return ret;
    },

    setLogger: function (logger) {
        this.logger = logger;
    },

    setEvents: function (events) {
        this.events = events;
    },
    
    setEnable: function ( enable ) {
        this.enable = enable;
    },

    addService: function (service, options={} ) {
        this.services[service.id] = {
            enable: service.enforcement.enable,
            config: service,
            policySet: {
                //target: { "request:path": service.url },
                apply: service.enforcement.apply || this.options.apply || 'permit-overrides',
                policies: []
            }
        };

        this.addServicePolicies(service, options);
    },

    addServicePolicies: function (service, options={}) {
        var self = this;

        if( options && options.reset){
            self.services[service.id].policySet.policies = [];
        }
        
        service.enforcement && Array.isArray(service.enforcement.policies) && service.enforcement.policies.forEach(function (policy) {
            try {
                var pol = self.retrievePolicy(policy);
                self.services[service.id].policySet.policies = self.services[service.id].policySet.policies.concat(pol);
            } catch (err) {
                self.logger.error(err, " error retrieving service policy ", policy);
            }
        });

    },

    setup: function (options) {
        var self = this;

        if (!options) {
            options = require('./config.json');
        }

        Schemas.assert(options, Schemas.schemas.register_options);

        this.options = _.defaults(options, this.defaults);

        // Register default data retrievers
        this.dataRetriever = new DataRetrievalRouter();

        this.dataRetriever.register('credentials', require('./dataRetrievers/credentials'));
        this.dataRetriever.register('connection', require('./dataRetrievers/connection'));
        this.dataRetriever.register('query', require('./dataRetrievers/query-params'));
        this.dataRetriever.register(['param', 'params'], require('./dataRetrievers/url-params'));
        this.dataRetriever.register('request', require('./dataRetrievers/request'));

        // Load user defined data retrievers
        this.options.dataRetrievers.forEach(function (dataRetrieverItem) {
            self.dataRetriever.register(dataRetrieverItem.handles, dataRetrieverItem.handler);
        });

        this.addEnforcementPolicies(this.options);
    },

    addEnforcementPolicies: function (options) {
        var self = this;

        if (options.reload) {
            self.policies = [];
        }

        Array.isArray(options.policies) && options.policies.forEach(function (policy) {
            try {
                var pol = self.retrievePolicy(policy);
                self.policies = self.policies.concat(pol);
            } catch (err) {
                self.logger.error(err, " error retrieving enforcement policy ", policy);
            }
        });

        // KLUDGE: Convert targets to regular expression
        this.policies.forEach((policy) => {
            if (policy.target) {
                if (!Array.isArray(policy.target)) { policy.target = [policy.target]; }
                policy.target.forEach((target) => {
                    Object.keys(target).forEach((key) => {
                        if (_.isObject(target[key])) {
                            // RegExp expression
                            target[key] = new RegExp(target[key].regexp, target[key].modifiers);
                        }
                    });            
                });
            }
            policy.rules.forEach((rule) => {
                if (rule.target) {
                    if (!Array.isArray(rule.target)) { rule.target = [rule.target]; }
                    rule.target.forEach((target) => {
                        Object.keys(target).forEach((key) => {
                            if (_.isObject(target[key])) {
                                // RegExp expression
                                target[key] = new RegExp(target[key].regexp, target[key].modifiers);
                            }
                        });            
                    });
                }
            });  
        });

    },

    reloadPolicies: function (options) {
        var self = this;

        //TODO: Stop the incoming requests during reload process

        self.policies = [];

        // Setup Enforcement
        this.setup(options.enforcement);

        // Add service policies
        Array.isArray(options.services) && options.services.forEach(function (service) {
            service.reload = this.reload;

            //TODO: Persistent the services
            self.addService(service, { reset: true });
        });
    },

    allow: function allow(serviceID, request, callback) {
        var self = this,
            service,
            policy;

        if (!this.enable) {
            callback(undefined, self.results.permit);
            return;
        }

        service = this.services[serviceID];
        if (!service) {
            callback && callback(new Error("Service " + serviceID + " not registered."));
            return;
        }

        if (!service.enable) {
            callback(undefined, self.results.permit);
            return;
        }

        // POR AKI
        policy = {
            // target: { "request:path": request.path },
            // target: service.policySet.target,
            apply: this.options.apply || service.policySet.apply,
            policies: service.policySet.policies.concat(this.policies || [])
        };

        // Add context to data retriever's child
        const wrappedDataRetriever = self.dataRetriever.createChild(request);

        RbacCore.evaluatePolicy(policy, wrappedDataRetriever, function (err, result) {
            if (err) {
                callback(err);
                return;
            }

            if (result === RbacCore.DENY) {
                callback(undefined, self.results.deny);
                return;
            }

            if (result === RbacCore.UNDETERMINED) {
                callback(undefined, self.results.undetermined);
                return;
            }
            callback(undefined, self.results.permit);
        });
    }
};