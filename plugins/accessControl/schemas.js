/**
 * Created by urkizu on 8/8/16.
 */

const Joi = require('joi');

var schemas = {};

schemas.policyRetriever = Joi.func();
schemas.policy = Joi.object();
schemas.DataRetrievalRouter_register_handles = Joi.alternatives().try(
    Joi.string().min(1),
    Joi.array().items(Joi.string().min(1))
);

/**
 * Register function
 **/
schemas.register_options = Joi.object(
    {
        onError: Joi.func().optional(),
        responseCode: Joi.object(
            {
                onDeny: Joi.number().optional(),
                onUndetermined: Joi.number().optional()
            }
        ).optional(),
        apply: Joi.string().optional(),
        enable: Joi.boolean().optional(),
        policies: Joi.array().items(
            schemas.policyRetriever,
            schemas.policy,
            Joi.string()
        ).optional(),
        reload: Joi.boolean().optional(),
        dataRetrievers: Joi.array().items(
            Joi.object(
                {
                    handles: schemas.DataRetrievalRouter_register_handles.required(),
                    handler: Joi.func().required()
                }
            )
        ).optional()
    }
);

exports.schemas = schemas;
exports.assert = Joi.assert;