
'use strict';

const Logger    = require('./logger');

module.exports = function setup(options, imports, register, app) {
    const logger = new Logger();
    logger.setUtilService(imports.util);
    logger.setup(options);

    register(
        null, // Error
        {
            log: {
                /**
                 * Get available log levels
                 * 
                 * @returns [array]
                 *      the list of log levels 
                */
                getLogLevels: logger.getLogLevels.bind(logger),
                /**
                 * Log message on debug level 
                 * 
                 * @param msg {string}
                 *      the mesagge to log 
                */
                debug: logger.debug.bind(logger),
                /**
                 * Log message on info level
                 * 
                 * @param msg {string}
                 *      the mesagge to log 
                */
                info: logger.info.bind(logger),
                /**
                 * Log message on warn level 
                 * 
                 * @param msg {string}
                 *      the mesagge to log 
                */
                warn: logger.warn.bind(logger),
                /**
                 * Log message on error level
                 * 
                 * @param msg {string}
                 *      the mesagge to log 
                */
                error: logger.error.bind(logger),
                /**
                 * Subscribe to events emitted by logger, for now only message on levels
                 * 
                 * @param level {string}
                 *      the level to subscribe
                 * @param callback {function}
                 *      callback to call with the message
                 */
                on: logger.on.bind(logger)
            }
        }
    );
};