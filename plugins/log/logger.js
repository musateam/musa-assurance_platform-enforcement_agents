
"use strict";

const EventEmitter      = require('events');
const winston           = require('winston');
const path              = require('path');
class MyEmitter extends EventEmitter { }

var Logger = module.exports = function () {
    this.emitter = new MyEmitter();
    this.transports = {
        'console': { name: 'musa-console' },
        'file': { name: 'musa-file', filename: path.join(process.cwd(), 'musa-agent.log') },
        'http': { name: 'musa-http' }        
    }
    this.logLevels = {
        debug: 0,
        info: 1,
        warn: 2,
        error: 3,
        silly: 4
    };
    winston.setLevels(this.levels);
};

Logger.prototype = {
    setup: function ( options ) {
        const self = this;

        (options.transports|| [{name: 'console', options: {}}, {name: 'file', options:{}}]).forEach( function (logger) {
            switch (logger.name){
                case 'console':
                    winston.remove(winston.transports.Console);
                    winston.add(winston.transports.Console, self.util.deepMerge(self.transports['console'], logger.options));                     
                    break;
                case 'file':
                    const options = self.util.deepMerge(self.transports['file'], logger.options);
                    if( !path.isAbsolute(options.filename) ) {
                        options.filename = path.join( process.cwd(), options.filename);
                    }
                    winston.add(winston.transports.File, options);                     
                    break;
                case 'http':
                    winston.add(winston.transports.Http, self.util.deepMerge(self.transports['http'], logger.options));                     
                    break;
                default:
                    break;
            }
        } );
    },
    setUtilService: function ( util ){
        this.util = util;
    },
    getLogLevels: function () {
        return Object.keys( logLevels );
    },
    debug: function () {
        winston.log.apply(winston, arguments);
        [].unshift.call(arguments, 'debug');        
        this.emitter.emit.apply(this.emitter, arguments);
    },
    info: function () {
        winston.info.apply(winston, arguments);
        [].unshift.call(arguments, 'info');        
        this.emitter.emit.apply(this.emitter, arguments);
    },
    warn: function () {
        winston.warn.apply(winston, arguments);
        [].unshift.call(arguments, 'warn');        
        this.emitter.emit.apply(this.emitter, arguments);
    },
    error: function () {
        winston.error.apply(winston, arguments);
        [].unshift.call(arguments, 'cerror');        
        this.emitter.emit.apply(this.emitter, arguments);
    },
    on: function (logLevel = logLevels, callback) {
        const self = this;

        !Array.isArray(logLevel) && (logLevel = [logLevel]);
        logLevel.forEach(function (log) {
            self.emitter.on(log, callback);
        });
    }
}