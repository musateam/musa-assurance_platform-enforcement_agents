
"use strict";

const util = require("./util");

module.exports = function startup(options, imports, register, app) {

    register(null, {
        util: {
            deepMerge: util.deepMerge
        }
    });
    
};
