<!-- TOC -->

- [Apps](#apps)
    - [MUSA Apps](#musa-apps)
        - [MUSA AccessControl](#musa-accesscontrol)
        - [MUSA IDentityManager](#musa-identitymanager)
- [Plugins](#plugins)
    - [BuiltIn plugins](#builtin-plugins)
- [Development](#development)
    - [App development](#app-development)
        - [App launcher](#app-launcher)
        - [App descriptor file](#app-descriptor-file)
    - [Plugin development](#plugin-development)
        - [Plugin configuration](#plugin-configuration)
        - [Plugin implementation](#plugin-implementation)
- [Author](#author)
- [License](#license)

<!-- /TOC -->

Each MUSA Agent is an app. This app is based on a plugins a simple but powerful plugin system. The plugin system it's easy to use and extend. 
## Apps
----------
Each app consists in a launcher and a descriptor file with plugins you want to load. Each plugin registers itself so other plugins can use its functions. Plugins can be maintained as NPM packages so they can be dropped in to other apps.
###  MUSA Apps
----------
There are two apps created for the project MUSA, an app is basically a descriptor file with a list of plugins plus configurations. 
#### MUSA AccessControl 
----------
The MUSA AccessControl app offer an access control to REST services based on XACML rules. In order to apply this rules each request must include a JWT token with the user info. If you use the IDentityManager app the JWT is created automatically and returned after the user signin process. 

To launch the app just need to use the launcher using the app descriptor
```
node agent.js -s app.ac.js
```
Full description document is located here [here](apps/ac/ac.md)
Integration guide is located here [here](apps/ac/integration.md)

#### MUSA IDentityManager 
----------
The MUSA IDentityManager agent offer a complete solution to manage identity lifecycle of your app. This implies signup, singin, signout process. Provide a JWT token ready to be used with AccessControl app.

To launch the app just need to use the launcher using the app descriptor
```
node agent.js -s app.idm.js
```
Full description document is located here [here](apps/idm/idm.md)
Integration guide is located here [here](apps/idm/integration.md)

## Plugins
----------
Each plugin is a node module complete with a package.json file. It need not actually be in npm, it can be a simple folder in the code tree.
### BuiltIn plugins
----------
Create a plugin is easy, however there is a list of built plugins that could be used

 - **logger**: a logger service that send log to stdout. The service name to use set in *consumes* config file is *logger*. See [logger's service readme](plugins/log/README.md) for more info about service's available functions
 - **events**: a events service to subscribe and emit events. The service name to use set in *consumes* config file is *events*. See [events's service readme](plugins/events/README.md) for more info about service's available functions
 - **broker**: a broker service to send and subscribe to kafka events. The service name to use set in *consumes* config file is *broker*. See [broker's service readme](plugins/broker/README.md) for more info about service's available functions
 - **proxy**: a proxy service to register http verbs on urls as middleware to intercep http requests. The service name to use set in *consumes* config file is *proxy*. See [proxy's service readme](plugins/proxy/README.md) for more info about service's available functions
 - **agent**: an agent service to manage the lifecycle or agent using a Finite State Machine. The service name to use set in *consumes* config file is *agent*. See [agent's service readme](plugins/agent/README.md) for more info about service's available functions
 - **jwt**: an jwt service to manage the lifecycle of jwt. The service name to use set in *consumes* config file is *jwt*. See [jwt's service readme](plugins/jwt/README.md) for more info about service's available functions

## Development

### App development 
----------
An app consists in a launcher that loads an app's descriptor file with 1..n plugins 
#### App launcher
----------
An app launcher must load app descriptor and then create app. The createApp function receives the descriptor and a  callback. This callback will be called once the app is ready or with error if any error ocurrs during the app ready process. 

An example of a command line app launcher:
```javascript
const yargs = require('yargs');
const path = require('path');
const architect = require('architect');

var argv = yargs
  .usage('This is agent launcher\n\nUsage: $0 [options]')
  .help('help').alias('help', 'h')
  .version()
  .options({
    app: {
      alias: 'a',
      description: "<filename> app descriptor file to load",
      requiresArg: true,
      required: true
    }
  })
  .argv;

console.log("Launching an agent using settings file " + path.join(__dirname, argv.settings));
const config = architect.loadConfig(path.join(__dirname, argv.settings));
architect.createApp(config, function (err, app) {
  if (err) {
    throw err;
  }
  console.log("Agent is ready");
});
```
This launcher will receive the following command line arguments:
 - **s|settings**:  relative path to app descriptor file to load
```bash
node agent.js -a <path.to.descriptor.file.js>
```

#### App descriptor file
----------
Each app has a descriptor file , this file can be either JSON or JS (or anything that node's require can read) and must return an array of plugins lo load. 

Each item of this array could be: 

 - **string**: an string with the path to the plugin location ( whereis plugin's package.json )
 - **object**: this object *must* have *packagePath* option and the other properties of the config will be passed as options to *setup* function 

A sampe app config file has a config like this:
```
module.exports = [
  { packagePath: "./plugins/http", port: 8080 },
  "./plugins/db",
  "./plugins/logger"
]
```
This app descriptor file will load 3 plugins, logger, db and  http ( that will receive an object { port: 8080 } as options on http's plugin setup function.

### Plugin development
----------
Each plugin consists in a plugin configuration and a plugin implementation.

#### Plugin configuration
----------
Each plugin has a package.json with the following structure: 
```    
{
        "name": "logger", // name of the plugin
        "version": "0.0.1", // version of the plugin 
        "main": "plugin.js", // main js that must implement the plublic interface of the service
        "plugin": {
            "consumes": ["database"], // an array with the list of service this plugin consumes 
            "provides": ["logger"] // the name of the service this plugin implements
        }
}
```
#### Plugin implementation
----------
Each plugin must register it's own interface. On app startup each plugin package.json main property is called with the following parameters: 

```
module.exports = function setup(options, imports, register) {
    register(
        null,
        {
            'service': // service this plugin provides
            { } // service implementation
        }
    );
}
```
The setup function receives three paramaters

 - **options** is the hash of options the user passes in when creating an instance of the plugin.
 - **imports** is a hash of all services this plugin consumes.
 - **register** is the callback to be called when the plugin is done initializ

Here is an example of a logger service:
```
/* All plugins must export this public signature.
ing.
*/
module.exports = function setup(options, imports, register) {
   // "database" was a service this plugin consumes
    var db = imports.database;
    register(
        null,
        {
            logger: // "logger" is a service this plugin provides
            {
                /**
                 * This plugin will expose this function as part os their service
                 */
                info: function (message) {
                    console.info(message);
                },
                saveEntryToDB: function (userID, entry, callback) {
                    db.set(userID, entry, function (error) {
                        if (!error) {
                            return callback(error);
                        }
                        return callback();
                    });
                }
            }
        }
    );
};
```
In this example a service **logger** is registered and uses other service **db** as a dependency. Its exposes 2 function, info ( syncronous function )  and saveEntryToDB ( asyncronous function )

## Author
----------
[Borja Urkizu Tecnalia](mailto:borja.urquizu@tecnalia.com )

## License
----------
