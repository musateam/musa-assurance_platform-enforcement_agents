const yargs = require('yargs');
const path = require('path');
const architect = require('./architect');

var argv = yargs
    .usage('This is agent launcher\n\nUsage: $0 [options]')
    .help('help').alias('help', 'h')
    .version()
    .options({
        app: {
            alias: 'a',
            description: "<filename> app descriptor file to load",
            requiresArg: true,
            required: true
        }
    })
    .argv;

console.log("Launching an agent using settings file " + path.join(__dirname, argv.app));

const config = architect.setup(__dirname, argv.app);
architect.createApp(config, function (err, app) {
    if (err) { throw err; }
    console.log("App is ready");
});
