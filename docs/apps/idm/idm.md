<!-- TOC -->

- [Getting started](#getting-started)
    - [Requirements](#requirements)
    - [Installation](#installation)
    - [Launch](#launch)
        - [Run locally without dashboard](#run-locally-without-dashboard)
- [Use Cases](#use-cases)
    - [App descriptor](#app-descriptor)
- [Integration](#integration)
    - [Create Auth0 account](#create-auth0-account)
    - [Configure MUSA IDentity Manager](#configure-musa-identity-manager)
    - [Configure WebApp to redirect authentication process to MUSA IDentity Manager](#configure-webapp-to-redirect-authentication-process-to-musa-identity-manager)
    - [Add every request the JWT Token](#add-every-request-the-jwt-token)
- [JWT Rest Service](#jwt-rest-service)
    - [Verify a token](#verify-a-token)
    - [Create a JWT Token](#create-a-jwt-token)
- [Issue Reporting](#issue-reporting)
- [TODO](#todo)
- [Author](#author)
- [License](#license)
- [Contributing](#contributing)

<!-- /TOC -->

MUSA IDentity Manager agent is a gateway to handle user lifecycle management. It provides a client to integrate with Identity Providers using OAuth2 and OpenID Connect protocols securely. 

It's provides the following features: 

 - Handle signup, signin, and signout processes 
 - Act as client in front of IDM providers 
 - Generate a JWT Token with user info inside it
 - JWT REST Service to verify token and sign payloads and return 

The only Identity Provider (idp) integrated yet is [Auth0](https://auth0.com/). This IDentity Provider could be use as a complete IDentity Provider, using Auth0 forms for signup, signin and signup process ans storing users on Auth0 storage. 

But if you are using your own user's storage (DB, LDAP, SAMLP IdentityProvider, MIcrosoft Azure AD,..) you can connect it to Auth0. Also you could extend with cool features like social signin ( google, twitter, facebook), passwordless (SMS, Email, TouchID)

## Getting started
----------
An overview of MUSA IDentity Manager App, how to download, configure and use. 

The singin process could be as follows:

 1. WebApp register a callback url to be redirect once user authentication process is done 
 2. User, throught browser, makes an authenticate request to WebApp
 3. WebApp redirect request to MUSA IDenetity Manager
 4. MUSA IDentity Manager it will redirect the browser to Auth0 IDentity Provider to let user authenticates against Auth0 ( or a Authentication mechanism registered on Auth0)
 5. The user authenticates herself and then MUSA IDentity Manager to the registered callback of WebApp with the user's JWT Token.
 
 Now if a user need to consume a Service protected with MUSA AccessControl the only things to do is send the token as HTTP Header on service request. If user has rigths to access the request will reach Service, otherwise will be rejected by MUSA AccessControl
 
 The process of signup is similar to the signin and will return a user's JWT Token to WebApp

### Requirements
* Kafka:
    - [kafka v 0.10.2.0](https://kafka.apache.org/quickstart) 
* Node:
    - [nodejs v6.3.1](https://nodejs.org/en/)
* DNS changes:
    - You need to change the hosts file, be sure to have rights to edit it:
``` bash
sudo nano /etc/hosts
127.0.0.1	kafka.enforcement.cu.cc kafka musa
127.0.0.1	backend-musa.enforcement.cu.cc idm-musa.enforcement.cu.cc webapp-musa.enforcement.cu.cc ac-musa.enforcement.cu.cc platform-musa.enforcement.cu.cc platform.enforcement.cu.cc
```

### Installation
First download the app via git:
```bash
git clone http://bitbucket.musa-project.eu:7990/scm/musa/musa-assurance_platform-enforcement_agents.git
cd musa-assurance_platform-enforcement_agents
```
Now install dependencies:
```bash
npm install
```
This will install all the app dependencies ( including plugins dependencies). 

### Launch
By default MUSA IDentity Manager will connect to a kafka instance on host `http://kafka.enforcement.cu.cc:2181`, this domain resolves to 127.0.0.1 so be sure that your kafka instance listens on that host:port or change it in the app descriptor file.

Launch MUSA IDentity Manager process:
```bash
npm run-script idm
```
or launch directly the app using node executable:

```bash
node agent.js -a app.idm.json
```

By default MUSA IDentity Manager is listening on port 3000, you can check pointing a browser to:
[https://idm-musa.enforcement.cu.cc:3000](https://idm-musa.enforcement.cu.cc:3000)

If everything is ok you will see the index page with **MUSA IDentity Manager App** title on it. 

#### Run locally without dashboard
Every agent communicate with a component called "Enforcement Platform" througth a message bus implemented using Kafka. With this `Enforcement Platform` you can change configuration, start and stop the agent. The default initial state of the agent, `initial` , don't serve the authorization service and need the `Enforcement Platform` to change it to the `started` state to serve the authorization service.

But if you want to test MUSA IDentityManager agent as a "normal" "local" server without the dependency of kafka and an instance of the "Enforcement Platform" you need to:
1. Mock the broker
2. Set initial state on agent to `started`

It can be done changing the config of two plugins of the agent, [agent](docs/plugins/agent) adding `mock = true` to the options section and [broker](docs/plugins/broker) adding `"initialState": [["initial", "idle"], ["awake", "awaked"], ["init", "inittited"], ["start", "started"]],` to the options section. 

For example this app descriptor will run the MUSA IDentityManager without kafka and an instance of the `Enforcement Platform`:

``` Javascript
{
    ...
        "broker": {
            "packagePath": "./plugins/broker",
            "options": {
                "mock": true,
                "hosts": "kafka.enforcement.cu.cc:2181"
            }
        },
        "agent": {
            "packagePath": "./plugins/agent",
            "options": {
                "agentID": "e5",
                "type": "IDentityManager",
                "initialState": [["initial", "idle"], ["awake", "awaked"], ["init", "inittiated"], ["start", "started"]],
                "keepAliveInterval": 300000
            }
        },
    ...
}
```

## Use Cases
----------
The most common use case of use of this app is to handle the authentication process and create JWT with authentication data without changing WebApp code.

 1. Configure IDentity Provider ( for now only [Auth0](http://auth0.com) is integrated ) by creating an account and configure it. Auth0 provides a widely range authentication mechanism. 
 2. Configure MUSA IDentity Manager app descriptor properly to set your auth0 credentials and JWT token urls
 2. Make changes to the client WebApp

Now MUSA IDentity Manager app descriptor is configured properly and all the signup/signin/signout process is done by MUSA IDentity Manager and the JWT Token is added to every request ( see section [Integration](#integration) )

### App descriptor
To configure the app, first we need to know the main settings we could set. The MUSA IDentity Manager app descriptor have the following plugins: 

```javascript
module.exports = [
    './plugins/log',
    './plugins/util',
    './plugins/events',
    Object.assign( 
        {
            packagePath: './plugins/broker'
        },
        {
            hosts: 'kafka.enforcement.cu.cc:2181'
        }
    ),
    Object.assign( 
        {
            packagePath: './plugins/proxy'
        },
        {
            "host": "0.0.0.0",
            "port": 3000
        }
    ),
    Object.assign(
        {
            packagePath: './plugins/agent'
        },
        {
            "agentID": "e1",
            "type": "IDentityManager",
            "initialState": "initial",
            "keepAliveInterval": 30000
        }
    ),
    Object.assign(
        {
            packagePath: './plugins/idm'
        },
        {
            "cookie_secret": "eljfwejewio282d4.3dPOLaw·q253%4wwqwqqs1&",
            "cookie_name": "musag",
            "idps": [
                {
                    "name": "auth0",
                    "domain": "musa.eu.auth0.com",
                    "idp_type": "auth0",
                    "clientID": "rrG4OMeKxmgjadY1HZn2zX6KEAz7XnBl",
                    "clientSecret": "hM9LSBYsw-U68xNsuDdAfOjDyFSI-cslT3a2-z_oajBOEGqh6_bdVlGmbueP29W0",
                    "signinPath": "/auth/auth0",
                    "signoutPath": "/auth/signout",
                    "passReqToCallback": true,
                    "jwt": {
                        "pem": "./idm/providers/musa.auth0.pem",
                        "cert": "./idm/providers/musa.auth0.cert",
                        "issuer": "https://musa.eu.auth0.com/"
                    },
                    "callbackURL": "https://idm-musa.enforcement.cu.cc:3000/callback"
                }
            ],
            "webapps": [
                {
                    "appID": "clientID_1",
                    "successRedirect": "http://webapp-musa.enforcement.cu.cc:3001/auth/account",
                    "failureRedirect": "http://webapp-musa.enforcement.cu.cc:3001/auth/fail",
                    "signoutRedirect": "http://webapp-musa.enforcement.cu.cc:3001/auth/post/signout"
                }
            ]
        }
    )
]
```
The MUSA IDentity Manager app incluye the following plugins:

 - **logger**: a logger service that send log to stdout. See [logger's service readme](plugins/logger/Readme.md) for more info
 - **events**: a events service to subscribe and emit events. See [events's service readme](plugins/events/Readme.md) for more info
 - **broker**: a broker service to send and subscribe to kafka events. 
	 - `hosts`:  where kafka-server is located. Ex: 'kafka.enforcement.cu.cc:2181'
See [broker's service readme](plugins/broker/Readme.md) for more info 
 - **proxy**: a proxy service to register http verbs on urls as middleware to intercep http requests. 
    - `host`: the host to expose the proxy server. Ex: "0.0.0.0"
    - `port`: the port to expose the proxy server. Ex: 3000
See [proxy's service readme](plugins/proxy/Readme.md) for more info 
 - **agent**: a broker service to send and subscribe to kafka events. 
	 - `agentID`: the id of the agent. This will be use to identify this agent on Enforcement Platform. Ex: "e1"
	 - `type`: the type of this agent. Ex: "IDentityManager"
	 - `initialState`: the initial state of the agent. Ex: "initial"
	 - `keepAliveInterval`: the interval of keep alive msg to send to Enforcement Platform. Ex: 30000
See [agent's service readme](plugins/agent/Readme.md) for more info
 - **idm**: the main plugin
	 - `cookie_secret`: secret to use on gateway session. Ex: "eljfwejewio282d4.3dPOLaw"
	 - `cookie_name`: name to use on gateway session. Ex: "musag"
	 - `idps`: An array with available authenticate providers. By default only Auth0 is available:
		 - **name**: name of the provider. Ex: "auth0"
		 - **idp_type**: the type of the idp. Ex: "auth0"
		 - **domain**: the domain of the auth0 project. Ex: "musa.eu.auth0.com"
		 - **clientID**: the clientID of the auth0 account. Ex: "rrG4OMeKxmgjadY1HZn2zX6KEAz7XnBl"
		 - **clientSecret**: the clientID of the auth0 account. Ex: "hM9LSBYsw-U68xNsuDdAfjofslT3a2"
		 - **signinPath**: the path to handle signin process. Ex: "/auth/auth0"
		 - **signoutPath**: the path to handle signout process. Ex: "/auth/signout"
		 - **passReqToCallback**: Ex: true
		 - **callbackURL**: After the user authenticates we will only call back to any of these URLs. Ex: "https://idm-musa.enforcement.cu.cc:3000/callback"
		 - **jwt**:  configuration of JWT Rest Service
            - `pem`: path to perm obtained from Auth0. Ex: "./idm/providers/musa.auth0.pem"
		    - `cert`: path to cert obtained from Auth0. Ex: "./idm/providers/musa.auth0.cert"
		    - `issuer`: url of the Auth0 JWT issuer. Ex: "https://musa.eu.auth0.com/
		 - **webapps**: An array of client webapp integrated. For each webapps registered we will need:
			 - `appID`: a client ID, each client will have a unique ID. Ex: clientID_1
			 - `successRedirect`: callback to redirect browser when signin process is sucessfully done. Ex: "http://webapp-musa.enforcement.cu.cc:3001/auth/account" 
			 - `failureRedirect`: callback to redirect browser when signin process is failure. Ex: "http://webapp-musa.enforcement.cu.cc:3001/auth/fail"
			 - `signoutRedirect`: callback to redirect browser when signout process is sucessfully done. Ex: "http://webapp-musa.enforcement.cu.cc:3001/auth/post/signout"
See [idm's service readme](plugins/idm/Readme.md) for more info

## Integration
----------
For example we will configure an WebApp express app to redirect all authentication processes to MUSA IDentity Manager app and consume a service protected by MUSA AccessControl app

 1. Create an account on Auth0 
 2. Configure MUSA IDentity Manager app to act as a client against Auth0
 3. Configure WebApp to redirect the authentication request to MUSA IDentity Manager
 4. Configure WebApp to add JWT token to service consume request

### Create Auth0 account 
To  process of creating an account on Auth0 

### Configure MUSA IDentity Manager 
**TODO**: Detail 
### Configure WebApp to redirect authentication process to MUSA IDentity Manager
**TODO**: Detail 
### Add every request the JWT Token
**TODO**: Detail 

## JWT Rest Service
----------
Is a REST Service with the following services:

### Verify a token
Verify the given JWT Token

 - **method**: POST
 - **path**: /verify  
 - **headers**:
    - `x-access-payload`: token to verify
    - `x-token-options`: settings to set verifying a token
        - **audience**: if you want to check audience (aud), provide a value here
        - **issuer** (optional): string or array of strings of valid values for the iss field.
        - **subject**: if you want to check subject (sub), provide a value here
 - **result**: the result of the operation
```javascript
{
    error: error,// if http code is not 200 any error ocurred
    result: boolean // true or false
}
```
### Create a JWT Token
There are no default values for expiresIn, notBefore, audience, subject, issuer. These claims can also be provided in the payload directly with exp, nbf, aud, sub and iss respectively, but you can't include in both places.

- **method**: POST 
- **path**: /sign
- **headers**:
    - `x-access-payload`: payload to encode as JWT. If payload is not a buffer or a string, it will be coerced into a string using JSON.stringify.
    - `x-token-options`: options to encode JWT. 
        - **cert**: certificate to use 
        - **expiresIn**: expressed in seconds or a string describing a time span zeit/ms. Eg: 60, "2 days", "10h", "7d". 
        - **notBefore**: expressed in seconds or a string describing a time span zeit/ms. Eg: 60, "2 days", "10h", "7d". 
        - **audience**: audience (aud) value. 
        - **issuer**: issuer (iss) value. 
        - **subject**: subject (sub) value. 
- **result**: the result of the operation
```javascript
{
    error: error,// if http code is not 200 any error ocurred
    result: token // true or false
}
```
    
## Issue Reporting
----------
If you have found a bug or if you have a feature request, please report them to this email borja.urquizu@tecnalia.com.

## TODO
----------
1. Add OAuth2 to protect API Gateway
2. Test & test and more test ( unit, integration at least )
3. Defensive programming everywhere
4. Protect JWT REST API it with OAuth2

## Author 
----------
[Borja Urkizu Tecnalia](mailto:borja.urquizu@tecnalia.com )

## License
----------

## Contributing 
----------


