
JWT App implements a REST Service to issue and verify a JWT Token and 

# Getting started
An overview of MUSA JWT  App, how to download, configure and use. 

## Requirements
* Node:
  * [nodejs v6.3.1](https://nodejs.org/en/)

## Installation
First download the app via git:
```bash
git clone http://bitbucket.musa-project.eu:7990/scm/musa/musa-assurance_platform-enforcement_agents.git
cd musa-assurance_platform-enforcement_agents
```
Now install dependencies:
```bash
npm install
```
This will install all the app dependencies ( including plugins dependencies). 

## Launch
Launch MUSA JWT Server process using npm:
```bash
npm run-script jwt
```
or launch directly the app using node executable:

```bash
node agent.js -a app.jwt.json
```

By default MUSA JWT is listening on port 5000, you can check pointing a browser to:
[https://127.0.0.1:5000](https://127.0.0.1:5000)

If everything is ok you will see the index page with **MUSA JWT Server** title on it. 

#### Run locally without dashboard
Every agent communicate with a component called "Enforcement Platform" througth a message bus implemented using Kafka. With this `Enforcement Platform` you can change configuration, start and stop the agent. The default initial state of the agent, `initial` , don't serve the authorization service and need the `Enforcement Platform` to change it to the `started` state to serve the authorization service.

But if you want to test MUSA JWT agent as a "normal" "local" server without the dependency of kafka and an instance of the "Enforcement Platform" you need to:
1. Mock the broker
2. Set initial state on agent to `started`

It can be done changing the config of two plugins of the agent, [agent](docs/plugins/agent) adding `mock = true` to the options section and [broker](docs/plugins/broker) adding `"initialState": [["initial", "idle"], ["awake", "awaked"], ["init", "inittited"], ["start", "started"]],` to the options section. 

For example this app descriptor will run the MUSA JWT without kafka and an instance of the `Enforcement Platform`:

``` Javascript
{
    ...
        "broker": {
            "packagePath": "./plugins/broker",
            "options": {
                "mock": true,
                "hosts": "kafka.enforcement.cu.cc:2181"
            }
        },
        "agent": {
            "packagePath": "./plugins/agent",
            "options": {
                "agentID": "e5",
                "type": "JWT",
                "initialState": [["initial", "idle"], ["awake", "awaked"], ["init", "inittiated"], ["start", "started"]],
                "keepAliveInterval": 300000
            }
        },
    ...
}
```

## App descriptor
The MUSA JWT App descriptor have the following plugins: 

```javascript
module.exports = [
    './plugins/log',
    './plugins/util',
    './plugins/events',
    Object.assign( 
        {
            packagePath: './plugins/proxy'
        },
        {
            "host": "0.0.0.0",
            "port": 5000
        }
    ),
    Object.assign(
        {
            packagePath: './plugins/jwt'
        },
        {
            "rest":{},
            "secret": "hM9LSBYsw-U68xNsuDdAfOjDyFSI-cslT3a2-z_oajBOEGqh6_bdVlGmbueP29W0",
            "issuer": "https://musa.eu.auth0.com/"
        }
    )
]
```
Each plugin has his own configuration:

 - **logger**: a logger service that send log to stdout. See [logger's service readme](plugins/logger/Readme.md) for more info
 - **events**: a events service to subscribe and emit events. See [events's service readme](plugins/events/Readme.md) for more info 
 - **proxy**: a proxy service to register http verbs on urls as middleware to intercep http requests. 
    - `host`: the host to expose the proxy server. Ex: "0.0.0.0"
    - `port`: the port to expose the proxy server. Ex: 3000
    - `headers`: the headers to retrieve from request
        - **token**: the header string that holds the user's jwt token
        - **provider**: the idp provider that issued token 
See [proxy's service readme](plugins/broker/Readme.md) for more info 
 - **jwt**: the main plugin of this app. This app exposes a REST Service to verify and sign JWT Tokens
    - `rest`: 
    - `secret`: 
    - `issuer`: 
See [jwt's service readme](plugins/jwt/Readme.md) for more info

## REST JWT Service
The IDentity Management app configures JWT plugin to expose a JWT REST Service  

### Verify a token

Verify a toke and returns the payload decoded if the signature (and, optionally audience, issuer) are valid. If not, it will return the error. This rest service only verify token signed by this JWT Rest Service

* **URL:**  
  `/jwt/verify`
  
* **Method:**  
  `POST`

* **BODY:**
```javascript
{
    "token": "efrwe32454354ojfe", // the JsonWebToken string
    "options": { //
        "audience": "audience",//if you want to check audience (aud), provide a value here
        "issuer": "issuer"//string or array of strings of valid values for the iss field to verify        
    }
}
``` 
* **Success Response:**  
Returns the payload decoded

  * **Code:** 200 <br />
    **Content:** `{ result: <payload> }`
 
* **Error Response:**
And error with the reason

  * **Code:** 403 UNAUTHORIZED <br />
    **Content:** `{ error : <reason> }`

* **Sample Call:**

  

### Sign a token

Returns the signed JsonWebToken as string If not, it will return the error. Generated jwts will include an iat (issued at) claim by default.

* **URL:**  
  `/jwt/sign`
  
* **Method:**  
  `POST`

* **BODY:**  
```javascript
    {
        "payload": {},
        "options": { 
            "expiresIn": "expiresIn",  //identifies the expiration time on or after which the JWT MUST NOT be accepted for processing expressed in seconds or a string describing a time span zeit/ms. Ex: Math.floor(Date.now() / 1000) + (60 * 60)
            "notBefore": "notBefore", //identifies the time on which the JWT will start to be accepted for processing. Expressed in seconds or a string describing a time span zeit/ms. Ex: Math.floor(Date.now() / 1000) + (60 * 60)
            "audience": "audience", //the recipients that the JWT is intended for. Each principal intended to process the JWT MUST identify itself with a value in the audience claim. If the principal processing the claim does not identify itself with a value in the aud claim when this claim is present, then the JWT MUST be rejected. 
            "subject": "subject", //identifies the subject  
            "jwtid": "jwtid"//case sensitive unique identifier of the token even among different issuers.             
        }
    } 
```

* **Success Response:**
  The JsonWebToken as string

  * **Code:** 200 <br />
    **Content:** `{ result : <JSONToken> }`
 
* **Error Response:**
And error with the reason

  * **Code:** 403 UNAUTHORIZED <br />
    **Content:** `{ error : <reason> }`

* **Sample Call:**


## Use Cases
The most common use case of this app is to `Create and sign a JWT Token` and `Verify a created JWT Token` using REST JWT Service

 1. Issue a JWT Token
 2. Verify a created JWT Token

### Issue a JWT Token
You need to consume the JWT Rest service using any method ( Postman, cURL,...) to isse a token. 
Ex: we want to generate a token with the following payload `{ "email": "musa-user@enforcement.cu.cc",  "rol": "user" }` so we will make a POST to the endpoint add a header with our payload. 
```bash
curl -X POST \
  https://idm-musa.enforcement.cu.cc:5000/auth0/jwt/sign \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
	"payload": { "email": "musa-user@enforcement.cu.cc",  "rol": "user" },
	"options": {}
}'
```
The response should be like this:
```javascript
HTTP 
HTTP/1.1 200 OK
X-Powered-By: Express
Access-Control-Allow-Origin: *
Content-Type: application/json; charset=utf-8
Content-Length: 278
ETag: W/"116-CNTXFNbleN1iucY7F13wfLTfU1s"
Date: Mon, 15 May 2017 10:12:00 GMT
Connection: keep-alive

{
    "result":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im11c2EtdXNlckBlbmZvcmNlbWVudC5jdS5jYyIsInJvbCI6InVzZXIiLCJleHAiOjE0OTQ4NDY3MjAsIm5iZiI6MTQ5NDg0MzEyMSwiaXNzIjoiaHR0cHM6Ly9tdXNhLmV1LmF1dGgwLmNvbS8iLCJpYXQiOjE0OTQ4NDMxMjB9.h-o0mmy3aRIqRScfeAye97OZe1_SpoP_eI3VuUVgVxg"
}
```
The result property is the JWT Token signed or a property with error ocurred

### Verify a JWT Token

```javascript
curl -X POST \
  https://idm-musa.enforcement.cu.cc:5000/auth0/jwt/verify \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 36afb861-506c-b5e6-12b8-dc41ecdb609a' \
  -d '{
	"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im11c2EtdXNlckBlbmZvcmNlbWVudC5jdS5jYyIsInJvbCI6InVzZXIiLCJleHAiOjE0OTQ4NDY3MjAsIm5iZiI6MTQ5NDg0MzEyMSwiaXNzIjoiaHR0cHM6Ly9tdXNhLmV1LmF1dGgwLmNvbS8iLCJpYXQiOjE0OTQ4NDMxMjB9.h-o0mmy3aRIqRScfeAye97OZe1_SpoP_eI3VuUVgVxg"
	
}'
```
The response should be like this:
```javascript
HTTP/1.1 200 OK
X-Powered-By: Express
Access-Control-Allow-Origin: *
Content-Type: application/json; charset=utf-8
Content-Length: 149
ETag: W/"95-dpyw5xpWA0hXwoHM0UVW9h00DTI"
Date: Mon, 15 May 2017 10:28:34 GMT
Connection: keep-alive

{
    "result":{
        "email":"musa-user@enforcement.cu.cc",
        "rol":"user",
        "exp":1494846558,
        "nbf":1494842959,
        "iss":"https://musa.eu.auth0.com/",
        "iat":1494842958
        }
}
```
The result is the token decoded or a property error with the error ocurred


## Contributing 
----------

## Issue Reporting
----------
If you have found a bug or if you have a feature request, please report them to this email borja.urquizu@tecnalia.com.
 
## TODO
----------

## Author
----------
[Borja Urkizu Tecnalia](mailto:borja.urquizu@tecnalia.com )

## License
