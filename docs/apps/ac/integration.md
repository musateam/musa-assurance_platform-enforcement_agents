<!-- TOC -->

- [Create JWT Token](#create-jwt-token)
    - [Using IDentity Management service](#using-identity-management-service)
        - [Create JWT Token](#create-jwt-token-1)
        - [Configure accessControl](#configure-accesscontrol)
    - [Using REST JWT Service](#using-rest-jwt-service)
        - [Create JWT Token](#create-jwt-token-2)
            - [Issued a JWT Token using curl](#issued-a-jwt-token-using-curl)
        - [Configure accessControl](#configure-accesscontrol-1)
    - [Using third party JWT Service](#using-third-party-jwt-service)
        - [Create JWT Token](#create-jwt-token-3)
        - [Configure accessControl](#configure-accesscontrol-2)
- [Add JWT token to HTTP request](#add-jwt-token-to-http-request)
- [Configure clients to enroute requests to service be redirect to MUSA AccessControl](#configure-clients-to-enroute-requests-to-service-be-redirect-to-musa-accesscontrol)
    - [Reverse Proxy](#reverse-proxy)
    - [Proxy](#proxy)
- [Create policies based on token payload](#create-policies-based-on-token-payload)
- [Contributing](#contributing)
- [Issue Reporting](#issue-reporting)
- [TODO](#todo)
- [Author](#author)
- [License](#license)

<!-- /TOC -->
Integration use cases 

 1. Create JWT Token  
 2. Configure clients to enroute requests to service be redirect to MUSA AccessControl
 3. Add JWT token to HTTP request 

# Create JWT Token 
Create JWT Token and configure Access Control to use JWT Token. Each generated token could have a payload (JSON strucuture) wich each property can be used on policies using as attribute. 

See [Create policies based on token payload](#create-policies-based-on-token-payload)

## Using IDentity Management service
----------
If app integrates the IDentity Management a token issued by Auth0 is returned to app and is verified by access Control app without any modification.
### Create JWT Token
The payload properties received on each token are:
- **email**: email of the user. The attibute will be `credentials:email`
- **rol**: rol of the user. The attibute will be `credentials:rol`
This properties can be added/changed in Auth0 account server.

Each property can be used on policies using as attribute. See [Create policies based on token payload](#create-policies-based-on-token-payload)

### Configure accessControl 
The default configuration will be enought, no modification to access control configuration are needed

## Using REST JWT Service
----------
You could use MUSA JWT Token Service, the JWT REST Service is described here [jwt's service readme](plugins/jwt/Readme.md)
### Create JWT Token
To create a token you must consume the REST Service using any method ( Postman, cURL,...) the API is not protected yet so you not need apikey/apisecret to access. 
* **URL:**  
  `https://<jwt-service-host>:5000/jwt/sign`
* **Method:**  
  `POST`
* **BODY:**  
```javascript
    {
        "payload": {},
        "options": { 
            "expiresIn": "expiresIn",  //identifies the expiration time on or after which the JWT MUST NOT be accepted for processing expressed in seconds or a string describing a time span zeit/ms. Ex: Math.floor(Date.now() / 1000) + (60 * 60)
            "notBefore": "notBefore", //identifies the time on which the JWT will start to be accepted for processing. Expressed in seconds or a string describing a time span zeit/ms. Ex: Math.floor(Date.now() / 1000) + (60 * 60)
            "audience": "audience", //the recipients that the JWT is intended for. Each principal intended to process the JWT MUST identify itself with a value in the audience claim. If the principal processing the claim does not identify itself with a value in the aud claim when this claim is present, then the JWT MUST be rejected. 
            "subject": "subject", //identifies the subject  
            "jwtid": "jwtid"//case sensitive unique identifier of the token even among different issuers.             
        }
    } 
```
The payload must be a JSON string with property and values to include on the signed JWT Token. This 

By default the JWT is configured to create tokens using IDentity Management service keys so access control will be configured by default to verify this tokens. 

You need to consume the JWT Rest service using any method ( Postman, cURL,...) to isse a token.
#### Issued a JWT Token using curl 
Ex: we want to generate a token with the following payload `{ "email": "musa-user@enforcement.cu.cc",  "rol": "user" }` so we will make a POST to the endpoint add a header with our payload. 
```bash
curl -X POST \
  https://idm-musa.enforcement.cu.cc:5000/auth0/jwt/sign \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
	"payload": { "email": "musa-user@enforcement.cu.cc",  "rol": "user" },
	"options": {}
}'
```
The response should be like this:
```javascript
HTTP 
HTTP/1.1 200 OK
X-Powered-By: Express
Access-Control-Allow-Origin: *
Content-Type: application/json; charset=utf-8
Content-Length: 278
ETag: W/"116-CNTXFNbleN1iucY7F13wfLTfU1s"
Date: Mon, 15 May 2017 10:12:00 GMT
Connection: keep-alive

{
    "result":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im11c2EtdXNlckBlbmZvcmNlbWVudC5jdS5jYyIsInJvbCI6InVzZXIiLCJleHAiOjE0OTQ4NDY3MjAsIm5iZiI6MTQ5NDg0MzEyMSwiaXNzIjoiaHR0cHM6Ly9tdXNhLmV1LmF1dGgwLmNvbS8iLCJpYXQiOjE0OTQ4NDMxMjB9.h-o0mmy3aRIqRScfeAye97OZe1_SpoP_eI3VuUVgVxg"
}
```
The result property is the JWT Token signed or a property with error ocurred

> You could change the JWT Token configuration to use your own keys. See [jwt's descriptor readme](apps/jwt/jwt.md)
### Configure accessControl 
The default configuration will be enought, no modification to access control configuration are needed

## Using third party JWT Service
----------
You could use your own service to create JWT Token 
### Create JWT Token
Depends on the third party service you must use this service to create a JWT Token
### Configure accessControl 
You must configure accessControl agent to verify the token. The agent could verify token using two differents modes: 
- **direct**: verify token locally. Must configure accessControl JWT plugin as follows
```javascript
{
    'mode': 'direct', 
    'pem': './certs/musa.pem', // path to public key to verify each JWT Token 
    'issuer': 'https://musa.eu.auth0.com/' // issuer of the token, used to verify iss claim on JWT Token
}
```

- **remote**: verify token using REST service. The third party service must provide a REST endpoint to verify the token.To configure accessControl JWT plugin to reach this as follows
```javascript
{
    'mode': 'remote', 
    'remote': {
        'host': 'https://musa.eu/jwt', // Host of the third party
        'headers': {
            'token': 'X-access-token', // Header to set token
            'options': 'X-token-options' // Header to add options
        },
        'verify': '/verify',
        'sign': '/sign'
    }
}
```
Depending on third party service the built remote client could not be enought.


# Add JWT token to HTTP request 
On every request the accessControl agent extract from http headers the token to use ( by default search token on "X-access-token" header ). The token header MUST be found.

Access control plugin take the token and do the following steps on it: 
1. Verify the token (signature, audience, issuer and optional expiration, or issuer), depending of the JWT plugin config of agent, it will be done inside agent or using a REST service. 
2. Evaluate request against the policies using JWT Token payload as attributes see [Create policies based on token payload](#create-policies-based-on-token-payload)



# Configure clients to enroute requests to service be redirect to MUSA AccessControl
It's depends on the framework/server used by the application 
## Reverse Proxy
A common way to do it is as a reverse proxy. So redirect all the request to service to the port of MUSA AccessControl App ( default port 4000 ) and configure MUSA AccessControl app descriptor to redirect all request to host service url. It's done by set in proxy plugin a *remote* property to the host url, 'http://backend-musa.enforcement.cu.cc:3002' ( not the service )
```javascript
module.exports = [
    './plugins/log',
    './plugins/util',
    './plugins/events',
    ...
    Object.assign( { packagePath: './plugins/proxy' },  
        {
            "host": "0.0.0.0",
            "port": 4000,
            "remote": "http://backend-musa.enforcement.cu.cc:3002"
            "headers": [
                {
                    "name": "token",
                    "value": "X-access-token"
                }
            ]
        }
    )
]    
```
This configuration of agent to set what will be the remote host to protect could be done using:
- before agent is deployed 
- using dashboard 
## Proxy
Configure Client/Server to use accessControl as a traditional proxy, so any request will be reach accessControl app and if the request is evaluated as permit will be forwarded to destination url (a reverse proxy, another proxy, or the backend services )

# Create policies based on token payload
----------
To create policies you must use attributtes. To know how to create and use attributes on policies read before this guide. See [Access Control Agent's Readme](apps/ac/ac.md)

When creating a JWT Token we could use payload information. The payload information is a JSON with property:value pairs, the content of this JSON could be any info 
```javascript
{ 
    ...
    "user":1, 
    "rol": "rol1"
    ...
}
```
This token payload information will be used by access control as a xacml attribute, can be accessed in policies ( credentials::<property>). 

Ex: We want to allow a user with email 'user@user2.com' to GET service /services/1 and deny to all
Create token with payload 
```javascript
{ 
    email: 'user@user2.com' 
}
```    
and set a policy 
```javascript
"policies": [
{
    "target": {
        "request:path": "/services/1",
        "request:method": "GET",
    },
    "rules": [
        {
            "target": {
                "credentials:email": "user@user2.com"
            },
            "effect": "permit"
        },
        {
            "effect": "deny"
        }
    ]
}
]
```
The evaluation of policies will be a result permit, reject, undetermined

# Contributing 

# Issue Reporting
If you have found a bug or if you have a feature request, please report them to this email borja.urquizu@tecnalia.com.
 
# TODO

# Author
[Borja Urkizu Tecnalia](mailto:borja.urquizu@tecnalia.com )

# License

