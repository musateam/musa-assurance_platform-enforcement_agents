<!-- TOC -->

- [Getting started](#getting-started)
    - [Requirements](#requirements)
    - [Installation](#installation)
    - [Launch](#launch)
        - [Run locally without dashboard](#run-locally-without-dashboard)
- [Use Cases](#use-cases)
    - [App descriptor](#app-descriptor)
    - [Policies](#policies)
- [Setup Access Control](#setup-access-control)
    - [Create policies to protect API Backend service](#create-policies-to-protect-api-backend-service)
    - [Add it to app MUSA AccessControl app descripto](#add-it-to-app-musa-accesscontrol-app-descripto)
- [Contributing](#contributing)
- [Issue Reporting](#issue-reporting)
- [TODO](#todo)
- [Author](#author)
- [License](#license)

<!-- /TOC -->

As the XACML model supports and encourages the separation of the access decision, the point of use, and the management of the policies. In this implementation the access decision ( XACML PDP ) and the point of use ( XACML PEP ) resides on the same instance to improve the performance. The management of the policies ( XACML PAP ) is not handle inside this app 

It's provides the following features: 

 - A **XACML PEP** that intercepts access request and pass it to **XACML PDP** and ensure his decission  
 - A **XACML PDP** that evaluate policies
 - A **XACML PIP** than handles attributes implemented as dataRetreiver it's implemented as dataRetreivers that get info from request (origin, date, ip,...), user (email,name,...)
 - JWT client to verify JWT tokens

## Getting started
----------
An overview of MUSA Access Control App, how to download, configure and use. 

For example we have a API Backend service, for each request sended by clients to consume this service, request will be:

 1. Intercepted by MUSA AccessControl App
 2. Extract user information from JWT Token located in request's header 
 3. Get context data from request 
 4. Make access control decission based on policies, user info and context data
 5. If decision is permit it will be redirect to backend service
 6. If decision is not permit the request will be rejected 

### Requirements
* Kafka:
  * [kafka v 0.10.2.0](https://kafka.apache.org/quickstart) 
* Node:
  * [nodejs v6.3.1](https://nodejs.org/en/)
* DNS changes:
  * You need to change the hosts file, be sure to have rights to edit it:
``` bash
sudo nano /etc/hosts
127.0.0.1	kafka.enforcement.cu.cc kafka musa
127.0.0.1	backend-musa.enforcement.cu.cc idm-musa.enforcement.cu.cc webapp-musa.enforcement.cu.cc ac-musa.enforcement.cu.cc platform-musa.enforcement.cu.cc platform.enforcement.cu.cc
```
### Installation
First download the app via git:
```bash
git clone http://bitbucket.musa-project.eu:7990/scm/musa/musa-assurance_platform-enforcement_agents.git
cd musa-assurance_platform-enforcement_agents
```
Now install dependencies:
```bash
npm install
```
This will install all the app dependencies ( including plugins dependencies). 

### Launch
By default MUSA AccessControl  will connect to a kafka instance on host `http://kafka.enforcement.cu.cc:2181` , this domain resolves to 127.0.0.1 so be sure that your kafka instance listens on that host:port or change it on the descriptor file.

Launch MUSA AccessControl process using npm:
```bash
npm run-script ac
```
or launch directly the app using node executable:

```bash
node agent.js -a app.ac.json
```

By default MUSA AccessControl is listening on port 4000, you can check pointing a browser to:
[https://ac-musa.enforcement.cu.cc:4000](https://ac-musa.enforcement.cu.cc:4000)

If everything is ok you will see the index page with **MUSA Access Control App** title on it. 

#### Run locally without dashboard
Every agent communicate with a component called "Enforcement Platform" througth a message bus implemented using Kafka. With this `Enforcement Platform` you can change configuration, start and stop the agent. The default initial state of the agent, `initial` , don't serve the authorization service and need the `Enforcement Platform` to change it to the `started` state to serve the authorization service.

But if you want to test MUSA AccessControl agent as a "normal" "local" server without the dependency of kafka and an instance of the "Enforcement Platform" you need to:
1. Mock the broker
2. Set initial state on agent to `started`

It can be done changing the config of two plugins of the agent, [agent](docs/plugins/agent) adding `mock = true` to the options section and [broker](docs/plugins/broker) adding `"initialState": [["initial", "idle"], ["awake", "awaked"], ["init", "inittited"], ["start", "started"]],` to the options section. 

For example this app descriptor will run the MUSA AccessControl without kafka and an instance of the `Enforcement Platform`:

``` Javascript
{
    ...
        "broker": {
            "packagePath": "./plugins/broker",
            "options": {
                "mock": true,
                "hosts": "kafka.enforcement.cu.cc:2181"
            }
        },
        "agent": {
            "packagePath": "./plugins/agent",
            "options": {
                "agentID": "e5",
                "type": "AccessControl",
                "initialState": [["initial", "idle"], ["awake", "awaked"], ["init", "inittiated"], ["start", "started"]],
                "keepAliveInterval": 300000
            }
        },
    ...
}
```

## Use Cases
----------
The most common use case of use of this app is to protect and API Backend Services from unapropiated used. 

 1. Create policies to protect API Backend services and add it to app MUSA AccessControl app descriptor
 2. Configure clients to enroute requests to service be redirect to MUSA AccessControl 
 
Now MUSA AccessControl app descriptor is configured properly and any request to backend service will be intercepted by MUSA AccessControl ( see section [Integration](#integration) )
 
### App descriptor
The MUSA IDM Agent app descriptor have the following plugins: 

```javascript
module.exports = [
    './plugins/log',
    './plugins/util',
    './plugins/events',
    Object.assign( { packagePath: './plugins/broker' },
        {
            hosts: 'kafka.enforcement.cu.cc:2181'
        }
    ),
    Object.assign( { packagePath: './plugins/proxy' },  
        {
            "host": "0.0.0.0",
            "port": 4000,
            "headers": [
                {
                    "name": "provider",
                    "value": "X-access-provider"
                },
                {
                    "name": "token",
                    "value": "X-access-token"
                }
            ]
        }
    ),
    Object.assign( { packagePath: './plugins/agent' },
        {
            "agentID": "e2",
            "type": "AccessControl",
            "initialState": "initial",
            "keepAliveInterval": 30000
        }
    ),
    Object.assign( { packagePath: './plugins/accessControl' },
        {
            "jwtissuer": "https://idm-musa.enforcement.cu.cc:3000",
            "enforcement": {
                "enabled": true,
                "apply": "permit-overrides",
                "policies": [
                    "./policies/"
                ]
            }
        }
    )
]
```
Each plugin has his own configuration:

 - **logger**: a logger service that send log to stdout. See [logger's service readme](plugins/logger/Readme.md) for more info
 - **events**: a events service to subscribe and emit events. See [events's service readme](plugins/events/Readme.md) for more info 
 - **broker**: a broker service to send and subscribe to kafka events. 
	 - `hosts`:  where kafka-server is located. Ex: 'kafka.enforcement.cu.cc:2181'
See [broker's service readme](plugins/broker/Readme.md) for more info 
 - **proxy**: a proxy service to register http verbs on urls as middleware to intercep http requests. 
    - `host`: the host to expose the proxy server. Ex: "0.0.0.0"
    - `port`: the port to expose the proxy server. Ex: 3000
    - `headers`: the headers to retrieve from request
        - **token**: the header string that holds the user's jwt token
        - **provider**: the idp provider that issued token 
See [proxy's service readme](plugins/broker/Readme.md) for more info 
 - **agent**: a broker service to send and subscribe to kafka events. 
	 - `agentID`: the id of the agent. This will be use to identify this agent on Enforcement Platform. Ex: "e1"
	 - `type`: the type of this agent. Ex: "IDentityManager"
	 - `initialState`: the initial state of the agent. Ex: "initial"
	 - `keepAliveInterval`: the interval of keep alive msg to send to Enforcement Platform. Ex: 30000
See [broker's service readme](plugins/broker/Readme.md) for more info
 - **accessControl**: the main plugin of this app. This service uses other services to implement access control
    - `proxy service`:  it uses this service to intercep all http requests
    - `agent service`: it uses this service to comunicate with the enforcement platform and react to enforcement platform commands
        - **start**: receives configuration an start to intercept http requests and apply access control
        - **stop**: stop this service, the request will not be intercepted [TBI]
        - **reload**: receives configuration an restart the services [TBI]
        - **setEnable**: intercept the request and bypass the policies and always permit        
See [accessControl's service readme](plugins/accessControl/Readme.md) for more info

### Policies
The heart of the accessControl are the policies, you must customize a set o policies to control the access to services. Our policy engine understand XACML policies but instead of XML version only understand JSON format. Basically is the same concepts but if you have a set of XACML rules writeen in XML you you need to rewrite them (or implement an parser)

The main concepts of this engine are: 

 - **policy**: It's represents restrictions to access a resource
 - **dataRetriever**: It's act's like XACML PIP and obtains the requested attributes by XACML PDP. An attribute is expresed on a policy as <dataRetreive:key>, where 'dataRetreive' is the dataRetreiver name an key is the property to search/get. You could create your own dataThere are a list of built in dataRetreiver ready to use as attributes in policies:
    - `connection`: it's provides connection attributes ( like property  origin,/get .You could create your own data.. ). 
    - `credentials`: it's provides credentials attributes ( email, name, ... ). For example  'credentials:email' 
    - `query`: it's provides query string attributes that appears in http request. For example  'query:someQueryStringParam' 
    - `params`: it's provides params string attributes that appears in http request. For example  'params:urlParam1' 
    - `request`: it's provides request attributes. For now only allowed attributes are 'request:method' and 'request:path'  

See [policies and dataRetreiver ](plugins/accessControl/policies.md) for more info

## Setup Access Control
----------
For example we will configure access control to protect an API Backend endpoint located on `http://backend-musa.enforcement.cu.cc:3002`. In this example only users that we need to protect. To protect it using MUSA AccessControl we need to do the following steps: 
 
 1. Create policies to protect API Backend service 
 2. Add it to app MUSA AccessControl app descriptor
 3. Configure clients to enroute requests to service be redirect to MUSA AccessControl
 4. Set JWT token on HTTP request 
 
### Create policies to protect API Backend service 
We want to protect a REST service /serviceToProtect/ against a POST method and allow access to ONLY users con rol developer AND with email bob@bob.com. It could be done with the builtin dataRetreivers. The default config of PDP is deny all and if any rule is evaluated as permit, overrides deny decision. 

So only need to create a policy that affect to service1 and then create a rule, one to deny access and another one to permit access to users with the role of developer and email 

```json
{
    "target": {
        "request:method": "/service1/"
    },
    "rules": [
        {
            "target": {
                "credentials:rol": "developer",
                "credentials:email": "bob@bob.com"
            },
            "effect": "permit"
        },
        {
            "effect": "deny"
        }
    ]
}
```

### Add it to app MUSA AccessControl app descripto
Now we need to configure MUSA Access Control App to take into account the policy created, there are two alternatives to do it:
1. Make a json file with this policy and put it inside the policy directory, /plugins/accessControl/policies/, you need to restart app or send reload command to app thought enforcement platform
2. Add it to app descriptor inside accessControl plugin configuration:
```javascript
module.exports = [
    './plugins/log',
    './plugins/util',
    './plugins/events',
    ...
    Object.assign( { packagePath: './plugins/accessControl' },
        {
            "jwtissuer": "https://idm-musa.enforcement.cu.cc:3000",
            "enforcement": {
                "enabled": true,
                "apply": "permit-overrides",
                "policies": [
                    "./policies/",
                    {
                        "target": {
                            "request:method": "/service1/"
                        },
                        "rules": [
                            {
                                "target": {
                                    "credentials:rol": "developer",
                                    "credentials:email": "bob@bob.com"
                                },
                                "effect": "permit"
                            },
                            {
                                "effect": "deny"
                            }
                        ]
                    }
                ]
            }
        }
    )
]
```

Now every request that reach the MUSA AccessControl app, ex POST http://ac-musa.enforcement.cu.cc:4000/service1/ will be evaluated against policies and redirect to http://backend-musa.enforcement.cu.cc:3002/service1/
  
## Contributing 
----------



## Issue Reporting
----------
If you have found a bug or if you have a feature request, please report them to this email borja.urquizu@tecnalia.com.
 
## TODO
----------
List of todo to be done:

 1. Add XAML PAP as a policy manager
    - Add custom protocol of events to manage add/remove/update of policies
    - Command line to manage PAP ( create policy)
 2. Extend XACML PIP adding more dataRetreivers ( connection, time, get data from external BBDD, .... )

## Author
----------
[Borja Urkizu Tecnalia](mailto:borja.urquizu@tecnalia.com )

## License
